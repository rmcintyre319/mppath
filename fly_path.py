# Fly a sequence of waypoints without GPS.  Waypoints are calculated
# by the pathfinder program using an evolutionary algorithm to find
# a path through a set of obstacles.  Navigation is done by calculating
# a heading and a time to fly that heading at the default velocity.
# Velocity commands are sent at regular period to maintain communication
# with the vehicle.

# Changes in this version: + Automatic takeoff is incorporated
#                          + Callback used to get ground location using GPS_RAW_INT
#                          + fly_waypoint_no_gps updated so that vehicle gets close
#                            using location calls and then flies blind for the last
#                            few meters (determined by an attribute
#                          + Code refactored using more methods to clean up Run()
#                            New methods: sim_arm_and_takeoff, preflight_routine,
#                            fly_mission

import math
import socket
import sys
import time

from dronekit import *

from inputs import load_input_data
from params import Params
from pathfinder import close_window, main, set_graphics
import utilities


# Handle MAVLink messages based on message type
def mav_message_handler(self, msg_name, msg):
    mtype = msg.get_type()
    # print "Received a message of type: %s" % mtype
    if mtype == 'GPS_RAW_INT':
        pass
        # RAW lat, lon values are ints with values * 1E7
        #     alt value is int (in meters) with value * 1E3
        # drone.ground_loc = Location(msg.lat/1E7, msg.lon/1E7, msg.alt/1E3, is_relative=False)
    elif mtype == 'LOCAL_POSITION_NED':
        print "LOCAL_POSITION_NED: %s" % msg
        # elif mtype == 'COMMAND_ACK':
        # pass
        # print "%s : %s" % (mtype, msg.command)
        # print "    result: %s" % msg.result
        # elif mtype == 'MISSION_ACK':
        # print "%s" % mtype
        # print "    result: %s" % msg.type


# Fly a genetic algorithm calculated mission
class GeneticPath(object):
    def __init__(self):

        # Connect to the vehicle
        # if simulation, connect to local,
        # otherwise, connect via USB
        if cmd_sim:
            # self.vehicle = connect('127.0.0.1:14550', wait_ready=True)  # sim_vehicle.sh simulator
            # self.vehicle = connect('tcp:127.0.0.1:5760', wait_ready=True)     # dronekit-sitl simulator
            self.vehicle = connect(connection_string, wait_ready=True)  # dronekit-sitl simulator
        else:
            # if computer is onboard Odroid, connect on serial
            if socket.gethostname() == 'odroid':
                self.vehicle = connect('/dev/ttyUSB0', baud=1500000, wait_ready=True)
            # otherwise, connect via radio
            else:
                self.vehicle = connect('/dev/ttyUSB0', baud=57600, wait_ready=True)

        print
        print "Connected to vehicle."

        self.ground_loc = None  # location on ground before takeoff
        self.home_loc = None  # home location after takeoff
        self.v_base = 5.0  # base velocity value in m/s
        self.alt_0 = 5  # takeoff altitude in meters
        self.dist_threshold = 1  # threshold for distance to waypoint
        # used in fly_waypoint_no_gps

        self.num_ga_attempts = 3  # number of attempts to find a path

        self.simulation = cmd_sim  # cmd_sim is a command line parameter

        self.max_time = 600  # max time in seconds
        self.min_alt = 0  # min altitude (absolute), 0 = none
        self.max_alt = 20  # max altitude, relative
        self.max_dist = 500  # max horizontal distance, meters NOT CURRENTLY USED

    def set_home_location(self):
        print
        print "Setting home location..."
        cmds = self.vehicle.commands
        cmds.download()
        cmds.wait_ready()
        # home_location is in Global frame with absolute altitude
        location = self.vehicle.home_location
        self.home_loc = LocationGlobal(location.lat, location.lon,
                                       location.alt)
        print "  home location:"
        print "    Latitude= {}  Longitude= {}  Altitude= {}\n\n".format(self.home_loc.lat, self.home_loc.lon,
                                                                         self.home_loc.alt)

    def preflight_routine(self):
        # Wait for Pixhawk boot completion
        print
        print "Basic pre-arm checks"
        while self.vehicle.mode.name == "INITIALISING":
            print "    Waiting for vehicle to initialise"
            time.sleep(1)

        # Wait for GPS fix
        print
        print "Waiting for GPS..."
        while self.vehicle.gps_0.fix_type < 3:
            # 0-1: no fix, 2: 2D fix, 3: 3D fix
            time.sleep(1)

        print "    GPS fix acquired..."
        print "        HDOP: {}cm\n".format(self.vehicle.gps_0.eph)

        # Wait for acquisition of starting position
        print
        print "Waiting for starting location..."
        # Get GPS location of vehicle on ground
        # If simulation, the location is determined by the 
        # location argument in the sim_vehicle.sh command
        # e.g. -L Branscomb
        while self.ground_loc is None:
            time.sleep(1)
            self.ground_loc = self.vehicle.location.global_relative_frame

        # After location is captured, callback no longer needed
        # self.vehicle.remove_message_listener(*, mav_message_handler)        

        print "    Starting location: [{}, {}, {}]".format(self.ground_loc.lat, self.ground_loc.lon,
                                                           self.ground_loc.alt)

        # wait for the is_armable parameter to be set subsequent to prearm checks
        print
        print "Waiting for is_armable parameter."
        while not self.vehicle.is_armable:
            time.sleep(1)
        print "    Vehicle is now armable."

    # SIMULATOR arm and takeoff
    def sim_arm_and_takeoff(self):
        # set mode to GUIDED and wait for result
        print
        print "Setting GUIDED mode..."
        self.vehicle.mode = VehicleMode("GUIDED")
        self.vehicle.flush()
        while self.vehicle.mode.name != "GUIDED":
            time.sleep(1)
        print "    In GUIDED mode..."

        # arm vehicle and wait for result
        print
        print "Arming motors..."
        self.vehicle.armed = True
        self.vehicle.flush()
        while not self.vehicle.armed:
            print "  Waiting for arming..."
            time.sleep(1)
        print "    Motors armed...\n"

        print
        print "Taking off..."
        self.vehicle.simple_takeoff(self.alt_0)  # Take off to 5m height

        self.vehicle.channel_override = {"3": 1500}
        self.vehicle.flush()

        # Wait until the vehicle reaches a safe height before processing the goto (otherwise the command 
        #  after vehicle.simple_takeoff will execute immediately).
        while True:
            print "  Altitude: ", self.vehicle.location.global_relative_frame.alt
            if self.vehicle.location.global_relative_frame.alt >= self.alt_0 * 0.95:
                # Just below target, in case of undershoot.
                print "    Reached target altitude"
                break
            time.sleep(1)

    # PHYSICAL VEHICLE arm and takeoff
    def arm_and_takeoff(self):
        # Set vehicle mode to GUIDED
        print
        print "Setting GUIDED mode..."
        # Copter should arm in GUIDED mode
        self.vehicle.mode = VehicleMode("GUIDED")

        while not self.vehicle.mode.name == "GUIDED":
            time.sleep(1)
        print "    In GUIDED mode"

        # Arm the motors
        print
        print "Arming motors..."
        self.vehicle.armed = True
        self.vehicle.flush()

        while not self.vehicle.armed:
            print "  Waiting for arming..."
            time.sleep(1)
        print "    Motors armed"

        time.sleep(2)
        print "About to takeoff..."
        # Takeoff
        print
        print "Taking off..."
        self.vehicle.simple_takeoff(self.alt_0)  # Take off to target altitude
        self.vehicle.flush()

        # Wait until the vehicle reaches a safe height before processing the goto (otherwise the command 
        #  after vehicle.simple_takeoff will execute immediately).
        while True:
            print "  Altitude: ", self.vehicle.location.global_relative_frame.alt
            if self.vehicle.location.global_relative_frame.alt >= self.alt_0 * 0.95:
                # Just below target, in case of undershoot.
                print "    Reached target altitude"
                break
            time.sleep(1)

    # Calculate Euclidean distance in meters between two points given
    # as [lat, lon]
    def distance_formula(self, point1, point2):
        return math.sqrt((((point2[0] - point1[0]) * utilities.meters_per_degree) ** 2) + (
            ((point2[1] - point1[1]) * utilities.meters_per_degree) ** 2))

    # Calculate the lat and lon differences in meters between two points
    # given in GPS coordinates by self.vehicle.location objects
    def distance_meters(self, loc_s, loc_t):
        lat_d = abs(loc_s.lat - loc_t.lat)  # lat diff in degrees
        lon_d = abs(loc_s.lon - loc_t.lon)  # lon diff in degrees
        lat_m = round((lat_d * utilities.meters_per_degree), 3)  # lat diff in meters
        lon_m = round((lon_d * utilities.meters_per_degree), 3)  # lon diff in meters
        euclid_m = round((math.sqrt(lat_m ** 2 + lon_m ** 2)), 3)
        return lat_m, lon_m, euclid_m

    # Fly to a waypoint without gps
    # This version updates vehicle location and calculates new
    # heading and distance at each time unit
    def fly_waypoint(self, waypt, index):
        print "Flying to waypoint %s..." % index
        # create the waypoint in global_relative_frame
        dest_point = LocationGlobalRelative(waypt[0], waypt[1], self.alt_0)
        # fly to the waypoint at specified velocity
        self.vehicle.simple_goto(dest_point, groundspeed=self.v_base)

        # calculate current location and distance to waypoint so
        # that we can determine when we have gotten close enough
        current_loc = self.vehicle.location.global_relative_frame
        dist_lat, dist_lon, dist = self.distance_meters(current_loc, dest_point)
        while dist > self.dist_threshold:
            time.sleep(0.25)
            current_loc = self.vehicle.location.global_relative_frame
            dist_lat, dist_lon, dist = self.distance_meters(current_loc, dest_point)

        print "    At waypoint %s..." % index

    # Fly the mission by flying to the waypoints one at a time
    # and then land the vehicle
    def fly_mission(self, mission_pts):
        print "Flying the mission..."
        print
        for i, waypt in enumerate(mission_pts):
            self.fly_waypoint(waypt, i + 1)

        # Get final position
        print "End of mission..."
        # Sleep to make sure velocity has settled
        time.sleep(1)
        final_loc = self.vehicle.location.global_relative_frame

        # Calculate and display the distance flown
        d_lat, d_lon, d_euclid = self.distance_meters(self.ground_loc, final_loc)
        print
        print("Final Position: Latitude= {}  Longitude= {}\n\n".format(final_loc.lat, final_loc.lon))
        print("Accuracy (m): lat= {0:.3f}  lon= {1:.3f}  euclidean= {2:.3f}\n".format(d_lat, d_lon, d_euclid))

        # Land
        print "Landing..."
        self.vehicle.mode = VehicleMode("LAND")
        # wait until vehicle is almost on ground
        while self.vehicle.location.global_relative_frame.alt > 0.5:
            print "  Altitude: ", self.vehicle.location.global_relative_frame.alt
            time.sleep(1)

        time.sleep(2)

    def run(self):
        # Notify user if simulation mode
        if self.simulation:
            print
            print "============================================================================"
            print "=====                        SIMULATION MODE                           ====="
            print "============================================================================"

        # Start mavlink message callback
        # self.vehicle.add_message_listener(*, mav_message_handler)

        # Wait for pre-arm checks to complete, GPS acquisition
        # and ground location.
        # Disable callback, if any, after ground location set.
        self.preflight_routine()

        # Load the input based on the input number entered at command line
        # This function lives in inputs.py
        load_input_data(cmd_input_num)

        # Set the starting position based on the ground location of the drone
        # (or the stored value if running a simulation)
        Params.start_point = [self.ground_loc.lat, self.ground_loc.lon]

        # Set the pathfinder graphics flag
        set_graphics(True)

        # main (defined in pathfinder.py) takes the number of attempts to make,
        # the population size and the number of generations.  
        # It then calls run_ga to calculate the waypoints.  The end location is 
        # loaded as part of the input case.
        print
        print "Finding path..."
        valid_path, mission_pts, testing_gens = main(self.num_ga_attempts, cmd_pop_size, cmd_gens)

        # If a path is found, prepare and then fly the mission
        if valid_path:
            print "    valid path found..."

            # Remove the first waypoint because it is the starting point
            mission_pts.pop(0)

            # Arm the drone and takeoff
            if self.simulation:
                self.sim_arm_and_takeoff()
            else:
                self.arm_and_takeoff()

            # Set home location to location at start of mission, including
            # altitude after takeoff
            self.set_home_location()

            # Fly the mission and land
            assert len(mission_pts) > 0
            self.fly_mission(mission_pts)

        # if no valid path, notify user and terminate
        else:
            print
            print "No valid path found.  MISSION ABORTED.\n"

        # close graphics window if applicaple
        if Params.GRAPHICS:
            print
            print "Closing graphics window."
            close_window()

        print
        print "Closing vehicle and terminating."
        print
        self.vehicle.close()

        if self.simulation:
            sitl.stop()


# read in command line arguments
numargs = len(sys.argv)
if numargs == 5:
    cmd_input_num = int(sys.argv[1])  # input obstacle course
    cmd_pop_size = int(sys.argv[2])  # population size
    cmd_gens = int(sys.argv[3])  # number of generations
    cmd_sim = (sys.argv[4] == 'True')  # [simulation mode - True if omitted]
elif numargs == 4:
    cmd_input_num = int(sys.argv[1])
    cmd_pop_size = int(sys.argv[2])
    cmd_gens = int(sys.argv[3])
    cmd_sim = True
else:
    sys.stderr.write("Usage: python {}  input popsize generations [simulation]\n".format(sys.argv[0]))
    raise SystemExit(1)

# Load the input based on the input number entered at command line
# This function lives in inputs.py
load_input_data(cmd_input_num)

# start the simulator with starting location determined by the input loaded above
if cmd_sim:
    import dronekit_sitl

    sitl = dronekit_sitl.start_default(lat=Params.start_point[0], lon=Params.start_point[1])
    connection_string = sitl.connection_string()

# create the drone instance
drone = GeneticPath()
drone.run()
