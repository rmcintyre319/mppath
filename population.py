import os
import time
from datetime import datetime
from random import randint

'''
try:
    from graphics import *
except ImportError:
    pass
'''

# GA imports
from cell import Cell
import utilities
from params import Params
from random import uniform


class Population:
    def __init__(self):
        self.population = []
        self.list_of_fronts = [[]]

    def add_random(self, num):
        # parameters
        max_waypoints = 6
        min_distance = 1

        '''
        start_end_dist = utilities.distance_formula(Params.start_point, Params.end_point)
        if start_end_dist == 0:
            max_distance = 200
        else:
            max_distance = 2 * start_end_dist
        '''

        midpoint = utilities.midpoint_formula(Params.start_point, Params.end_point)

        # while set size is less than population size
        while len(self.population) < num:
            # add start point to path
            random_cell = Cell([Params.start_point])

            # initial waypoints in range 2..6
            for i in xrange(0, randint(2, max_waypoints) + 1):
                random_cell.genes.append(utilities.rand_waypoint(Params.max_dist, min_distance, midpoint))

            # add end point to path
            random_cell.genes.append(Params.end_point)

            if Params.seed_valid:
                for obs in Params.obstacles:
                    random_cell.genes = obs.make_valid_path(random_cell.genes)

            # add path to self.population
            self.population.append(random_cell)

        assert len(self.population) == num

    def fast_nondomination_sort(self):
        front_index = 0

        for p in self.population:
            for q in self.population:
                if p.dominates(q):
                    p.cells_i_dominate.append(q)
                elif q.dominates(p):
                    p.my_domination_count += 1
            if p.my_domination_count == 0:
                p.front = front_index + 1
                self.list_of_fronts[front_index].append(p)

        while len(self.list_of_fronts[front_index]) != 0:
            self.list_of_fronts.append([])  # front_index + 1
            for p in self.list_of_fronts[front_index]:
                for q in p.cells_i_dominate:
                    q.my_domination_count -= 1
                    if q.my_domination_count == 0:
                        q.front = front_index + 2  # front count + 1 or "next front"
                        self.list_of_fronts[front_index + 1].append(q)  # add to next front
            front_index += 1
        assert len(self.list_of_fronts) <= len(self.population) + 1  # plus one for the empty front


    def crowding_distance_assignment(self, front_index):

        # front = self.list_of_fronts[front_index]
        front = self.population
        length = len(front)

        # all distances are set to 0 when fnds_datastructure is created
        # for objective in xrange(0, 5):  # hard coded for 5 objectives ("5" including obj 0 which is not used)
        # objs_array is a global array shared by all processes
        for objective in Params.objs_used:
            front.sort(key=lambda cell: cell.objectives[objective])
            # endpoints get automatic best crowding dist
            front[0].crowding_distance = float('inf')
            front[length - 1].crowding_distance = float('inf')
            # use formula to assign all other points a crowding distance
            for i in xrange(1, length - 1):
                denom = (
                    float(front[length - 1].objectives[objective]) - float(
                        front[0].objectives[objective]))
                if denom == 0:
                    denom = 1e-9
                    # print "Fire in the disco!"

                front[i].crowding_distance += (front[i + 1].objectives[objective] - front[i - 1].objectives[
                        objective]) / denom


    def save_population_log(self, today_date, input_number, generation, population_size):
        waypoint_count = 0
        path_len = 1
        obstacle_error = 2
        target_reward = 3
        smoothness = 4

        # check to see if log folder exists, else make one.
        if not os.path.exists('logs'):
            os.makedirs('logs')
        # check to see if date folder exists, else make it.
        if not os.path.exists('logs/{}'.format(today_date)):
            os.makedirs('logs/{}'.format(today_date))
        # open a new file and write the population to it, front by front
        time_stamp = datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
        with open("logs/{}/{}-in{}-pop{}-gen{}.txt".format(today_date, time_stamp, input_number, population_size,
                                                           generation),
                  "w") as population_log:
            for i, front in enumerate(self.list_of_fronts):
                population_log.write("\nFront {}\n".format(i + 1))
                for cell in front:
                    population_log.write(
                        "{} {} {} {} {} {} {}\n".format(cell.objectives[waypoint_count], cell.objectives[path_len],
                                                        cell.objectives[obstacle_error], cell.objectives[target_reward],
                                                        cell.objectives[smoothness], cell.crowding_distance,
                                                        cell.genes))

    def clear_domination_values(self):
        del self.list_of_fronts[:]
        self.list_of_fronts.append([])
        for cell in self.population:
            del cell.cells_i_dominate[:]
            cell.my_domination_count = 0
            cell.front = 0
            cell.crowding_distance = 0

    def binary_tournament(self):
        best = None
        # tournament pressure is the number of members competing for selection
        for i in xrange(Params.tournament_pressure):

            random_cell = self.population[randint(0, len(self.population) - 1)]
            # 0 -> rank 1-> distance
            if (best is None) or (
                        (random_cell.front < best.front) or ((random_cell.front == best.front) and (
                                random_cell.crowding_distance > best.crowding_distance))):
                best = random_cell
        assert best is not None
        return best

    def top_n(self, n):
        top_n = []
        front_index = 0
        while len(top_n) + len(self.list_of_fronts[front_index]) <= n:
            self.crowding_distance_assignment(front_index)
            top_n.extend(self.list_of_fronts[front_index])
            front_index += 1

        self.crowding_distance_assignment(front_index)
        self.list_of_fronts[front_index].sort(reverse=True, key=lambda cell: cell.crowding_distance)

        i = 0
        while len(top_n) < n:
            top_n.append(self.list_of_fronts[front_index][i])
            i += 1

        assert len(top_n) == n
        return top_n

    def extinction(self):
        population_size = len(self.population)
        new_parents = Population()

        if Params.extinction_type == 1:
            # Deletes 80% of population - completely random
            # No Elitism.  All new members are random.

            ###
            del_num = int(0.8 * population_size)
            if del_num % 2 == 1:
                del_num -= 1
            for i in xrange(del_num):
                victim = int(uniform(0, population_size - i))
                del self.population[victim]
            ###

            print "    {} killed.".format(del_num)

            new_parents.add_random(del_num)

            for cell in new_parents.population:
                cell.evaluate_path()

            self.population.extend(new_parents.population)

            assert len(self.population) == population_size

        elif Params.extinction_type == 2:
            # Called from Extinction implementation 2
            # Deletes 80% of pop with elitism of 5 members

            ###
            # keep the top 10% but no less than 2 members
            preserve = int(0.1 * population_size)
            if preserve < 2:
                preserve = 2

            # if the population is small, it may be impossible to kill 80% 
            # and keep at least 2 members, so adjust del_num in this case
            del_num = int(0.8 * population_size)
            if del_num > population_size - preserve:
                del_num = population_size - preserve
                
            if del_num % 2 == 1:
                del_num -= 1
            for i in xrange(del_num):
                victim = int(uniform(0, population_size - (i + preserve))) + preserve
                del self.population[victim]
            ###

            print "    {} killed.".format(del_num)

            random_create = int(del_num * 0.6)

            # create a population for the new parents so that we can evaluate them without reevaluating the old parents
            new_parents.add_random(random_create)

            for cell in new_parents.population:
                cell.evaluate_path()

            # add elite members to random members for storage
            new_parents.population.extend(self.population)

            while len(new_parents.population) < population_size:
                new_p1, new_p2 = self.binary_tournament().x_over(self.binary_tournament())
                if len(new_p1.genes) > 4:
                    new_p1.evaluate_path()
                    new_parents.population.append(new_p1)
                if len(new_p2.genes) > 4:
                    new_p2.evaluate_path()
                    new_parents.population.append(new_p2)

            # delete a member if the size of the parents became too big
            if len(new_parents.population) > population_size:
                new_parents.population.pop()

            self.population = new_parents.population

            assert len(self.population) == population_size

        elif Params.extinction_type == 3:
            # Extinction Version 3
            # keeps the best 10 members of the population

            ###
            del_num = int(len(self.population) * 0.9)
            keep_num = len(self.population) - del_num
            del self.population[keep_num:]
            ###

            print "    {} killed.".format(del_num)

            random_create = int(0.1 * del_num)
            new_parents.add_random(random_create)

            # complete the parent population via mutation of the top 10 members
            for i in xrange(del_num - random_create):
                new_parents.population.append(self.population[randint(0, keep_num - 1)].mutate())

            # evaluate each member of the new_parent population
            for cell in new_parents.population:
                cell.evaluate_path()

            self.population.extend(new_parents.population)

            assert len(self.population) == population_size

        elif Params.extinction_type == 4:
            # Extinction Version 4
            # keep all of front 0

            self.fast_nondomination_sort()

            print "Size of front 0: %s" % len(self.list_of_fronts[0])
            del self.population[:]
            self.population.extend(self.list_of_fronts[0])
            keep_num = len(self.population)
            del_num = population_size - keep_num
            # if the number of elements to be deleted is too small, recalculate
            if del_num < int(0.3 * population_size):
                del_num = int(0.3 * population_size)
                keep_num = population_size - del_num
                del self.population[keep_num:]

            print "    {} killed.".format(del_num)

            random_create = int(0.2 * del_num)

            new_parents.add_random(random_create)

            # complete the parent population via mutation of the top 10 members
            for i in xrange(del_num - random_create):
                new_parents.population.append(self.population[randint(0, keep_num - 1)].mutate())

            # evaluate each member of the new_parent population
            for cell in new_parents.population:
                cell.evaluate_path()

            self.population.extend(new_parents.population)

            assert len(self.population) == population_size

        else:
            print "ERROR: INVALID EXTINCTION PARAMETER!"

        self.clear_domination_values()
        self.fast_nondomination_sort()

        del new_parents

    def simplify_all(self):
        for cell in self.population:
            cell.simplify()
        return
