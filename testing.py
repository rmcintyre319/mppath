from inputs import load_input_data
from params import Params
from pathfinder import main
import utilities
import sys
import os
from datetime import datetime
from time import time
"""
    # crossover parameters
    p_xo = 1  # probability of crossover (1 or 0)
    p_xo_m = 0.5  # probability of mutation after crossover

    # extinction parameters
    p_ext = 0  # probability of extinction
    extinction_type = 1  # extinction type (value must be 1 2 or 3)

    # Cell.mutate() parameters
    p_m_add = 0.1  # probability for adding a waypoint
    p_m_del = 0.05  # probability for deleting a waypoint
    p_m_swap = 0.1  # probability for swapping two waypoints
    m_mu = 0  # used for the Gaussian distribution
    small_move = 0.00005  # standard dev. values for magnitude of move for existing waypoint
    big_move = 0.0002  # depends on input
"""

input_list = [34]

xo_type_list_actual = [3]
p_xo_list = [0.50]
p_xo_m_list_actual = [0]

p_ext_list = [0]
extinction_type_list = [0]

p_m_add_list = [0.25]
p_m_del_list = [0.15]
p_m_swap_list = [0.1]
m_mu_list = [0]
# small_move_list = [0.00005]
# big_move_list = [0.0004]

valid_seed_list = [False, True]
make_valid_list = [False, True]
valid_interval_list = [40]
obs_intrude_list = [True]
smooth_list = [False]

pop_size_list = [100]
avg_move_list = [18]


def print_test(s, i, t):
    print "\n\n** Series {} | Input {} | Test {} **\n\n".format(s, i, t)

if __name__ == "__main__":

    random_seed = None

    numargs = len(sys.argv)
    if numargs == 4:
        cmd_pop_size = int(sys.argv[1])
        cmd_gens = int(sys.argv[2])
        random_seed = int(sys.argv[3])
    elif numargs == 3:
        cmd_pop_size = int(sys.argv[1])
        cmd_gens = int(sys.argv[2])
    else:
        sys.stderr.write("Usage: python {} popsize generations (random_seed)\n".format(sys.argv[0]))
        raise SystemExit(1)

    '''
    if random_seed is None:
        Params.random_seed = int(round(time() * 1000))
    else:
        Params.random_seed = random_seed
    '''

    todays_date = datetime.fromtimestamp(time()).strftime('%Y-%m-%d')

    if not os.path.exists('logs'):
        os.makedirs('logs')
    if not os.path.exists("logs/{}".format(todays_date)):
        os.makedirs('logs/{}'.format(todays_date))

    '''
    # moved here since not using it for current tests
    for obs_intrude in obs_intrude_list:
        if obs_intrude:
            Params.objs_map_pre = [1, 1, 1, 1, 1, 0, 0, 1]
            Params.objs_map_post = [1, 1, 1, 1, 1, 0, 0, 1]
        else:
            Params.objs_map_pre = [1, 1, 1, 1, 1, 0, 0, 0]
            Params.objs_map_post = [1, 1, 1, 1, 1, 0, 0, 0]
        utilities.update_obj_array(Params.objs_map_pre)
    '''

    for series in xrange(10):
        # if series % 5 == 0:
        if random_seed is None:
            Params.random_seed_pop = int(round(time() * os.getpid()))
            Params.random_seed_run = None
        for input_num in input_list:
            csv_string = ""
            load_input_data(input_num)
            valid_list = []
            ten_list = []
            five_list = []
            time_list = []
            test_count = 0
            # for pop_size in pop_size_list:
                # cmd_pop_size = pop_size
            for valid_seed in valid_seed_list:
                Params.seed_valid = valid_seed
                for mv in make_valid_list:
                    Params.make_valid = mv
                    if mv:
                        int_list = valid_interval_list
                    else:
                        int_list = [0]
                    for valid_int in int_list:
                        Params.make_valid_interval = valid_int
                        for obs_intrude in obs_intrude_list:
                            if obs_intrude:
                                Params.objs_map_pre = [1, 1, 0, 0, 0, 1]
                            else:
                                Params.objs_map_pre = [1, 1, 0, 0, 0, 0]
                            utilities.update_obj_array(Params.objs_map_pre)
                            for p_xo in p_xo_list:
                                Params.p_xo = p_xo
                                if p_xo == 0:
                                    p_xo_m_list = [0]
                                    xo_type_list = [3]
                                else:
                                    p_xo_m_list = p_xo_m_list_actual
                                    xo_type_list = xo_type_list_actual
                                for xo_type in xo_type_list:
                                    Params.xo_type = xo_type
                                    for p_xo_m in p_xo_m_list:
                                        Params.p_xo_m = p_xo_m
                                        for extinction_type in extinction_type_list:
                                            Params.extinction_type = extinction_type
                                            for p_m_add in p_m_add_list:
                                                Params.p_m_add = p_m_add
                                                for p_m_del in p_m_del_list:
                                                    Params.p_m_del = p_m_del
                                                    for p_m_swap in p_m_swap_list:
                                                        Params.p_m_swap = p_m_swap
                                                        for m_mu in m_mu_list:
                                                            Params.m_mu = m_mu
                                                            for move in avg_move_list:
                                                                Params.m_avg_move = move
                                                                for pop in pop_size_list:
                                                                    cmd_pop_size = pop

                                                                    print_test(series, input_num, test_count)
                                                                    print('valid_seed = {0}  make_valid = {1}  valid_interval = {2}  obs_int = {3}'.format(valid_seed, mv, valid_int, obs_intrude))
                                                                    test_count += 1
                                                                    is_valid, best_mem, test_list, time_start, time_end = main(1, cmd_pop_size, cmd_gens)

                                                                    valid_list.append(test_list[0])
                                                                    ten_list.append(test_list[1])
                                                                    five_list.append(test_list[2])
                                                                    time_list.append(time_end - time_start)

            for val in valid_list:
                csv_string += ",{}".format(val)
            csv_string += ","
            for val in ten_list:
                csv_string += ",{}".format(val)
            csv_string += ","
            for val in five_list:
                csv_string += ",{}".format(val)
            csv_string += ","
            for val in time_list:
                csv_string += ",{}".format(val)
            csv_string += ",\n"
            with open('logs/{}/input{}.csv'.format(todays_date, input_num), 'a') as csv_file:
                csv_file.write(csv_string)
