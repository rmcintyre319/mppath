from inputs import load_input_data
from params import Params
from pathfinder import main
import sys
import os
from datetime import datetime
from time import time
"""
    # crossover parameters
    p_xo = 1  # probability of crossover (1 or 0)
    p_xo_m = 0.5  # probability of mutation after crossover

    # extinction parameters
    p_ext = 0  # probability of extinction
    extinction_type = 1  # extinction type (value must be 1 2 or 3)

    # Cell.mutate() parameters
    p_m_add = 0.1  # probability for adding a waypoint
    p_m_del = 0.05  # probability for deleting a waypoint
    p_m_swap = 0.1  # probability for swapping two waypoints
    m_mu = 0  # used for the Gaussian distribution
    small_move = 0.00005  # standard dev. values for magnitude of move for existing waypoint
    big_move = 0.0002  # depends on input
"""


def print_test(s, i, t):
    print "\n\n** Series {} | Input {} | Test {} **\n\n".format(s, i, t)

input_num = 20
gens_list = []
for series in xrange(3):
    Params.random_seed = int(round(time() * 1000))
    load_input_data(input_num)

    # print_test(series, input_num, test_count)
    print
    is_valid, best_mem, test_list = main(1, 100, 1000)

    gens_list.append(test_list[2])
    
print gens_list


