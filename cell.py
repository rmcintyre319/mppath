"""
Cell class was taken from the crossover branch which was more advanced than the master version
"""

import math
from copy import deepcopy
from random import randint, uniform, gauss, shuffle

# GA imports
import utilities
from params import Params


class Cell:
    def __init__(self, genes):
        self.genes = genes
        # 0: number of waypoints
        # 1: path length
        # 2: error for obstacles hit
        # 3: reward for targets hit
        # 4: smoothness
        # 5: obstacle intrusion

        self.objectives = []
        for i in xrange(len(Params.objs_map_pre)):
            self.objectives.append(0)

        self.cells_i_dominate = []
        self.my_domination_count = 0
        self.front = 0
        self.crowding_distance = 0

    # invokes a crossover version based on value of Params.xo_type
    def x_over(self, cell2):

        if Params.xo_type == 1:
            child1, child2 = self.x_over1(cell2)
        elif Params.xo_type == 2:
            child1, child2 = self.x_over2(cell2)
        elif Params.xo_type == 3:
            child1, child2 = self.x_over3(cell2)
        '''
        # random crossover selection (choices are 1 and 3 with 50/50 probability)
        rand_xo = randint(0, 2)
        if rand_xo == 0:
            child1, child2 = self.x_over1(cell2)
        elif rand_xo == 1:
            child1, child2 = self.x_over2(cell2)
        else:
            child1, child2 = self.x_over3(cell2)
        '''
        return child1, child2

    # first version of crossover.
    # chooses a random cutpoint in each parent
    def x_over1(self, cell2):
        parent1 = deepcopy(self.genes)
        parent2 = deepcopy(cell2.genes)

        # first possible cutpoint is before the first non-start waypoint
        # last possible cutpoint is before the end point
        cutpoint1 = randint(1, len(parent1) - 1)
        cutpoint2 = randint(1, len(parent2) - 1)

        child1 = []
        child1.extend(parent1[0:cutpoint1])
        child1.extend(parent2[cutpoint2:len(parent2)])

        child2 = []
        child2.extend(parent2[0:cutpoint2])
        child2.extend(parent1[cutpoint1:len(parent1)])

        assert len(parent1) + len(parent2) == len(child1) + len(child2)
        roll1 = uniform(0, 1)
        roll2 = uniform(0, 1)
        if roll1 < Params.p_xo_m and roll2 < Params.p_xo_m:
            return Cell(child1).mutate(), Cell(child2).mutate()
        elif roll1 < Params.p_xo_m:
            return Cell(child1).mutate(), Cell(child2)
        elif roll2 < Params.p_xo_m:
            return Cell(child1), Cell(child2).mutate()
        else:
            return Cell(child1), Cell(child2)

    # second version of crossover
    # attempts to find an intersection between path segments in the two parents
    # if not, it copies and mutates each parent
    def x_over2(self, cell2):
        parent1 = deepcopy(self.genes)
        parent2 = deepcopy(cell2.genes)
        parent1_order = range(1, len(parent1) - 2)
        shuffle(parent1_order)
        parent2_order = range(1, len(parent2) - 2)
        shuffle(parent2_order)

        for i in parent1_order:
            for j in parent2_order:
                p1_1 = parent1[i]
                p1_2 = parent1[i + 1]
                p2_1 = parent2[j]
                p2_2 = parent2[j + 1]
                # if utilities.is_intersection(p1_1, p1_2, p2_1, p2_2):
                # print "Intersection occurred."

                cp1_1 = [p1_1[1], p1_1[0]]
                cp1_2 = [p1_2[1], p1_2[0]]
                cp2_1 = [p2_1[1], p2_1[0]]
                cp2_2 = [p2_2[1], p2_2[0]]

                # print "p1 segment: %s ---- %s" % (cp1_1, cp1_2)
                # print "p2 segment: %s ---- %s" % (cp2_1, cp2_2)
                int_type, int_point = utilities.find_intersection_point(cp1_1, cp1_2, cp2_1, cp2_2)
                if int_type in [2, 5, 6, 7]:
                    # print "    Crossover occurred\n"

                    Params.crossover_count += 1
                    child1 = []
                    child2 = []

                    child1.extend(parent1[0:i + 1])
                    child1.extend(parent2[j + 1:len(parent2)])

                    child2.extend(parent2[0:j + 1])
                    child2.extend(parent1[i + 1:len(parent1)])

                    # roll dice to see if children will be mutated
                    roll1 = uniform(0, 1)
                    roll2 = uniform(0, 1)

                    if roll1 < Params.p_xo_m and roll2 < Params.p_xo_m:
                        return Cell(child1).mutate(), Cell(child2).mutate()
                    elif roll1 < Params.p_xo_m:
                        return Cell(child1).mutate(), Cell(child2)
                    elif roll2 < Params.p_xo_m:
                        return Cell(child1), Cell(child2).mutate()
                    else:
                        return Cell(child1), Cell(child2)

        # if made it through loops without performing crossover,
        # mutate both parents
        # print "    Mutation occurred\n"
        Params.mutation_count += 1
        child1 = Cell(parent1).mutate()
        child2 = Cell(parent2).mutate()
        return child1, child2

    # third version of crossover
    # similar to crossover 2 but handles intersection points differently
    # adds all intersection point to both children in all cases
    """ Return values from find_intersection_point:
        0: no segment intersection (no point)
        1: equal segments (no point)
        2: share one endpoint CROSSOVER (shared_endpoint)
        3: share one endpoint NO CROSSOVER (shared_endpoint)
        4: co-linear (no point)
        5: seg1 endpoint on seg2 (seg1 endpoint)
        6: seg2 endpoint on seg1 (seg2 endpoint)
        7: classical intersection (x, y)
    """

    def x_over3(self, cell2):
        parent1 = deepcopy(self.genes)
        parent2 = deepcopy(cell2.genes)
        parent1_order = range(1, len(parent1) - 2)
        shuffle(parent1_order)
        parent2_order = range(1, len(parent2) - 2)
        shuffle(parent2_order)

        for i in parent1_order:
            for j in parent2_order:
                p1_1 = parent1[i]
                p1_2 = parent1[i + 1]
                p2_1 = parent2[j]
                p2_2 = parent2[j + 1]

                cp1_1 = [p1_1[1], p1_1[0]]
                cp1_2 = [p1_2[1], p1_2[0]]
                cp2_1 = [p2_1[1], p2_1[0]]
                cp2_2 = [p2_2[1], p2_2[0]]
                # print "p1 segment: %s ---- %s" % (p1_1, p1_2)
                # print "p2 segment: %s ---- %s" % (p2_1, p2_2)
                int_type, int_point = utilities.find_intersection_point(cp1_1, cp1_2, cp2_1, cp2_2)
                if int_type in [2, 5, 6, 7]:
                    if int_type == 2:
                        # print "    Crossover type 2 occurred\n"
                        Params.crossover_count += 1
                        child1 = []
                        child2 = []

                        child1.extend(parent1[0:i + 1])
                        child1.extend(parent2[j + 1:len(parent2)])

                        child2.extend(parent2[0:j + 1])
                        child2.extend(parent1[i + 1:len(parent1)])

                    elif int_type == 5:
                        # print "    Crossover type 5 occurred\n"
                        # print "    Intersection point: %s" % [int_point[1], int_point[0]]
                        Params.crossover_count += 1
                        child1 = []
                        child2 = []

                        if int_point == cp1_1:
                            # intersection point is parent1[i]
                            child1.extend(parent1[0:i + 1])
                            child1.extend(parent2[j + 1:len(parent2)])

                            child2.extend(parent2[0:j + 1])
                            child2.extend(parent1[i:len(parent1)])
                        else:
                            # intersection point is parent1[i+1]
                            child1.extend(parent1[0:i + 2])
                            child1.extend(parent2[j + 1:len(parent2)])

                            child2.extend(parent2[0:j + 1])
                            child2.extend(parent1[i + 1:len(parent1)])

                    elif int_type == 6:
                        # print "    Crossover type 6 occurred\n"
                        # print "    Intersection point: %s" % [int_point[1], int_point[0]]
                        Params.crossover_count += 1
                        child1 = []
                        child2 = []

                        if int_point == cp2_1:
                            # intersection point is parent2[j]
                            child1.extend(parent1[0:i + 1])
                            child1.extend(parent2[j:len(parent2)])

                            child2.extend(parent2[0:j + 1])
                            child2.extend(parent1[i + 1:len(parent1)])
                        else:
                            # intersection point is parent2[j+1]
                            child1.extend(parent1[0:i + 1])
                            child1.extend(parent2[j + 1:len(parent2)])

                            child2.extend(parent2[0:j + 2])
                            child2.extend(parent1[i + 1:len(parent1)])

                    elif int_type == 7:
                        # print "    Crossover type 7 occurred\n"
                        Params.crossover_count += 1
                        child1 = []
                        child2 = []

                        child1.extend(parent1[0:i + 1])
                        child1.append([int_point[1], int_point[0]])
                        child1.extend(parent2[j + 1:len(parent2)])

                        child2.extend(parent2[0:j + 1])
                        child2.append([int_point[1], int_point[0]])
                        child2.extend(parent1[i + 1:len(parent1)])

                    # roll dice to see if children will be mutated
                    roll1 = uniform(0, 1)
                    roll2 = uniform(0, 1)
                    if roll1 < Params.p_xo_m and roll2 < Params.p_xo_m:
                        return Cell(child1).mutate(), Cell(child2).mutate()
                    elif roll1 < Params.p_xo_m:
                        return Cell(child1).mutate(), Cell(child2)
                    elif roll2 < Params.p_xo_m:
                        return Cell(child1), Cell(child2).mutate()
                    else:
                        return Cell(child1), Cell(child2)

        # if made it through loops without performing crossover,
        # mutate both parents
        # print "    Mutation occurred\n"
        Params.mutation_count += 1
        child1 = Cell(parent1).mutate()
        child2 = Cell(parent2).mutate()
        return child1, child2

    def mutate(self):
        cell = deepcopy(self.genes)
        roll = uniform(0, 1)
        sigma_move = (Params.m_avg_move / (math.sqrt(2 / math.pi))) * utilities.degrees_per_meter

        if roll < Params.p_m_add:
            # add a waypoint at the midpoint of a path segment
            # select two adjacent waypoints
            wp1 = randint(0, (len(cell) - 2))
            wp2 = wp1 + 1
            # calculate the midpoint
            midpoint = utilities.midpoint_formula(cell[wp1], cell[wp2])
            # mutate the midpoint
            for i, axis in enumerate(midpoint):
                # increment = gauss(mu, sigma_add)
                increment = gauss(Params.m_mu, sigma_move)
                midpoint[i] = axis + increment
            midpoint = utilities.fix_mutation(midpoint)
            cell.insert(wp2, midpoint)
        elif roll < (Params.p_m_add + Params.p_m_del) and len(cell) > 3:
            # delete
            index = randint(1, len(cell) - 2)
            del cell[index]
        elif roll < (Params.p_m_add + Params.p_m_del + Params.p_m_swap) and len(cell) > 3:
            # swap
            wp1 = randint(1, (len(cell) - 3))
            wp2 = wp1 + 1
            cell.insert(wp1, cell.pop(wp2))
        else:
            # choose a random waypoint and mutate it
            if len(cell) > 2:
                index = randint(1, len(cell) - 2)
                for i, axis in enumerate(cell[index]):
                    cell[index][i] = axis + gauss(Params.m_mu, sigma_move)
                cell[index] = utilities.fix_mutation(cell[index])

        # Uncommenting this causes every mutated cell to be valid
        '''
        if Params.make_valid:
            return (Cell(cell)).make_valid()
        else:
            return Cell(cell)

        # return Cell(cell)
        '''
        return Cell(cell)

    def dominates(self, cell2, max_fn=False):
        # does self dominate cell2
        # by default we use a minimization function

        # the following algorithm tests if cell2 is dominated by self
        # therefore the order of the cells appears reversed but logically it is correct

        if max_fn:
            all_leq = True
            one_lss = False
            # for objective in xrange(0, 5):
            # objs_array is a global array shared by all processes
            for objective in Params.objs_used:
                all_leq = all_leq and (cell2.objectives[objective] <= self.objectives[objective])
                one_lss = one_lss or (cell2.objectives[objective] < self.objectives[objective])
            return all_leq and one_lss
        else:  # min
            all_geq = True
            one_gtr = False
            # for objective in xrange(0, 5):
            # objs_array is a global array shared by all processes
            for objective in Params.objs_used:
                all_geq = all_geq and (cell2.objectives[objective] >= self.objectives[objective])
                one_gtr = one_gtr or (cell2.objectives[objective] > self.objectives[objective])
            return all_geq and one_gtr

    def evaluate_path(self):
        # eval objectives for a path

        # clears eval data to prevent parent reeval bugs
        self.objectives = []
        for i in xrange(len(Params.objs_map_pre)):
            self.objectives.append(0)

        # we always want to calculate number of waypoints so it is done prior to loop
        calculate_num_waypoints(self)

        # we always want to calculate path_len so it is done prior to loop
        calculate_path_length(self)

        # for each objective that is currently being used, call the corresponding function
        # from the list of functions (defined at bottom of this file).
        for i in xrange(2, len(Params.objs_array)):
            if Params.objs_array[i]:
                function_list[i](self)


    def simplify(self):
        i = 0
        while i < len(self.genes) - 2:
            if self.genes[i] == self.genes[i + 1]:
                del self.genes[i + 1]
                i -= 1
            i += 1
        return


    # make this member valid with respect to all obstacles
    def make_valid(self):
        for obstacle in Params.obstacles:
            self.genes = obstacle.make_valid_path(self.genes)
        return self


def calculate_num_waypoints(self):
    self.objectives[Params.waypoint_count] = len(self.genes) - 2


def calculate_path_length(self):
    # if Params.path_len in Params.objs_array:  # commented because we want to calculate path_len even if not used
    # self.objectives[Params.path_len] = 0
    for i in xrange(len(self.genes) - 1):
        self.objectives[Params.path_len] += utilities.distance_formula(self.genes[i], self.genes[i + 1])


def calculate_obstacle_error(self):
    # for each index from 0 to length-1
    bitmap_obstacles = []
    for i in xrange(len(Params.obstacles)):
        bitmap_obstacles.append(0)

    for i in xrange(len(self.genes) - 1):
        x1 = self.genes[i][1]  # lon
        y1 = self.genes[i][0]  # lat
        x2 = self.genes[i + 1][1]  # lon
        y2 = self.genes[i + 1][0]  # lat
        obstacle_bitmap_counter = 0
        for ob in Params.obstacles:
            ob = ob.legacy_data
            if Params.no_obstacle_double_count and bitmap_obstacles[obstacle_bitmap_counter] == 1:
                obstacle_bitmap_counter += 1
                continue
            if len(ob) == 3:
                xc = ob[1]  # lon
                yc = ob[0]  # lat
                ob_radius = ob[2]  # radius
                if (((y2 - y1) ** 2) + ((x2 - x1) ** 2)) != 0:
                    t = (((yc - y1) * (y2 - y1)) + ((xc - x1) * (x2 - x1))) / (
                        ((y2 - y1) ** 2) + ((x2 - x1) ** 2))
                else:
                    t = float('inf')
                if 0 < t < 1:
                    intersection = [y1 + (t * (y2 - y1)), x1 + (t * (x2 - x1))]  # lat, lon
                    if utilities.distance_formula(ob, intersection) <= ob_radius:
                        self.objectives[Params.obstacle_error] += 1
                        bitmap_obstacles[obstacle_bitmap_counter] = 1
                elif utilities.distance_formula(ob, [y1, x1]) <= ob_radius or \
                                utilities.distance_formula(ob, [y2, x2]) <= ob_radius:
                    self.objectives[Params.obstacle_error] += 1
                    bitmap_obstacles[obstacle_bitmap_counter] = 1
            # if the obstacle is a line segment (point point)
            elif len(ob) == 4:
                # if intersect_2d_2segments([x1,y1],[x2,y2],[ob[1],ob[0]],[ob[3],ob[2]]) > 0:
                if utilities.is_intersection([x1, y1], [x2, y2], [ob[1], ob[0]], [ob[3], ob[2]]):
                    self.objectives[Params.obstacle_error] += 1
                    bitmap_obstacles[obstacle_bitmap_counter] = 1
            # if the obstacle is a polygon (point, point, ... point) !at least 6 data points!
            else:
                # add code for polygon intersection
                if utilities.poly_intersection([x1, y1], [x2, y2], ob):
                    self.objectives[Params.obstacle_error] += 1
                    bitmap_obstacles[obstacle_bitmap_counter] = 1

            obstacle_bitmap_counter += 1


def calculate_target_reward(self):
    dummy_reward = -1000
    decay_power = 2

    for goal in Params.goals:
        xc = goal[1]  # lon
        yc = goal[0]  # lat
        goal_radius = goal[2]  # radius
        minimum_distance = float('inf')
        for i in xrange(len(self.genes) - 1):
            x1 = self.genes[i][1]  # lon
            y1 = self.genes[i][0]  # lat
            x2 = self.genes[i + 1][1]  # lon
            y2 = self.genes[i + 1][0]  # lat
            if (((y2 - y1) ** 2) + ((x2 - x1) ** 2)) != 0:
                t = (((yc - y1) * (y2 - y1)) + ((xc - x1) * (x2 - x1))) / (((y2 - y1) ** 2) + ((x2 - x1) ** 2))
            else:
                t = float('inf')
            if 0 < t < 1:
                intersection = [y1 + (t * (y2 - y1)), x1 + (t * (x2 - x1))]  # lat, lon
                minimum_distance = min(utilities.distance_formula(intersection, goal), minimum_distance)
            else:
                minimum_distance = min(
                    min(utilities.distance_formula(goal, [y1, x1]), utilities.distance_formula(goal, [y2, x2])),
                    minimum_distance)
        # Use scaling for goals -- this results in faster convergence
        # than a reward of -1 for each goal
        if minimum_distance <= goal_radius:
            self.objectives[Params.target_reward] += dummy_reward
        else:
            self.objectives[Params.target_reward] += dummy_reward / (
                1 + ((minimum_distance - goal_radius) ** decay_power))


def calculate_smoothness(self):
    angle_sum = 0
    if len(self.genes) > 2:
        for i in xrange(len(self.genes) - 2):
            x1 = self.genes[i][1]  # lon
            y1 = self.genes[i][0]  # lat
            x2 = self.genes[i + 1][1]  # lon
            y2 = self.genes[i + 1][0]  # lat
            x3 = self.genes[i + 2][1]  # lon
            y3 = self.genes[i + 2][0]  # lat
            seg12_x = x1 - x2
            seg12_y = y1 - y2
            seg23_x = x3 - x2
            seg23_y = y3 - y2
            dot_prod = utilities.dot_product([seg12_x, seg12_y], [seg23_x, seg23_y])
            magnitude12 = utilities.distance_formula_gps(self.genes[i], self.genes[i + 1])
            magnitude23 = utilities.distance_formula_gps(self.genes[i + 1], self.genes[i + 2])
            # subtract angle from pi to give "straighter" angles small values
            # and sharper angles large values because we want to favor straighter
            # angles in our crowding calculation
            # print dot_prod, magnitude12, magnitude23, (magnitude12 * magnitude23)
            # print dot_prod, magnitude12, magnitude23, (magnitude12 * magnitude23), ratio
            if (magnitude12 * magnitude23) == 0:
                ratio = dot_prod / 1e-10
            else:
                ratio = dot_prod / (magnitude12 * magnitude23)
            if 1.001 > ratio > 1.0:  # we hypothesize that roundoff error is causing problems.
                ratio = 1.0  # we are only allowing small errors, large errors are caught by the assert
            if -1.001 < ratio < -1.0:  # we hypothesize that roundoff error is causing problems.
                ratio = -1.0
            assert -1 <= ratio <= 1  # this checks that the ratio is in the domain of acos
            angle_sum += (math.pi - math.acos(ratio)) ** 2
    self.objectives[Params.smoothness] = angle_sum



def obstacle_intrusion_sum(self):
    for obstacle in Params.obstacles:
        # self.objectives[Params.obstacle_intrusion] += obstacle.path_intrusion(self.genes)
        intrusion_value = obstacle.path_intrusion(self.genes)
        if intrusion_value >= 0:
            self.objectives[Params.obstacle_intrusion] += intrusion_value
        else:
            print "Negative intrusion value."
            for i in xrange(len(self.objectives)):
                self.objectives[i] = 1e8
            break


# list of functions for calculating objective values
# This allows use of a simple loop to call each of the
# objective calculation functions in turn
function_list = [calculate_num_waypoints, calculate_path_length, calculate_obstacle_error,
                 calculate_target_reward, calculate_smoothness, obstacle_intrusion_sum]
