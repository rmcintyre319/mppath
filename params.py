from multiprocessing import Array


class Params:
    def __init__(self):
        pass

    # intersection_count_array = [0, 0, 0, 0, 0, 0, 0, 0]  # TODO DEBUG
    crossover_count = 0
    mutation_count = 0

    # objective list index aliasing
    waypoint_count = 0
    path_len = 1
    obstacle_error = 2
    target_reward = 3
    smoothness = 4
    obstacle_intrusion = 5

    # enables or disables graphic visuals
    GRAPHICS = False

    # bitmap for objectives to use -- indices are defined above

    objs_map_pre = [1, 1, 1, 0, 0, 1]

    objs_used = list()

    # Array (not a list) of objectives used for evaluation
    # Initially waypoint_count is used as an objective
    # When a valid solution is found, waypoint_count is replace with path_len
    # THIS IS SHARED BY ALL PROCESSES
    objs_array = Array('i', objs_map_pre, lock=False)

    # sort priority for radix sorting (least priority to greatest)
    # sort_order = [waypoint_count, smoothness, path_len, target_reward]
    sort_order = [waypoint_count, path_len, obstacle_error, obstacle_intrusion]
    # sort_order = [waypoint_count, smoothness, path_len, obstacle_error, target_reward]

    # double count obstacles or not
    no_obstacle_double_count = True

    # tournament pressure for choosing members to crossover or mutate
    # set to 1 for random choice
    tournament_pressure = 2

    # seed for generating population
    random_seed_pop = None

    # seed for the GA run
    random_seed_run = None

    # input data and parameters
    start_point = []  # start point
    end_point = []  # end point
    max_dist = 0
    goals = []  # goals
    obstacles = []  # obstacles
    early_termination_threshold = -1  # early termination threshold (value of -1 disables early term)
    threshold_percent_buffer = 1.05  # what percent of optimal should the termination threshold be
    origin = []
    scale_factor = 3.75  # scaling factor for graphics window (Mr. Georges's Green is default)

    area_equator = [26.0, -84.0]

    # crossover parameters
    p_xo = 0.5  # probability of crossover (1 to 0) float
    p_xo_m = 0   # probability of mutation after crossover
    xo_type = 3  # which crossover to use:
    # 1 is original
    # 2 is extra crispy (based on intersection of path segments)
    # 3 is extra crispy with intersection point included in child paths

    # extinction parameters
    interval_ext = 40  # generations between possible extinction events
    inc_ext = 0.05  # amount to increase p_ext when no extinction event occurs
    p_ext_init = 0.1  # base extinction probability
    p_ext = p_ext_init  # probability of extinction
    extinction_type = 0  # extinction type (0 means don't use extinction. valid types 1-4)

    # Cell.mutate() parameters
    p_m_add = 0.25  # probability for adding a waypoint
    p_m_del = 0.15  # probability for deleting a waypoint
    p_m_swap = 0.1  # probability for swapping two waypoints
    m_mu = 0  # used for the Gaussian distribution
    m_avg_move = 8

    # set to true to correct each member of the population to avoid obstacles at initial seeding process
    seed_valid = False

    # make members valid as they are created
    make_valid = False
    make_valid_interval = 100

    refine_gens = 0
    
