import math
from copy import deepcopy
from random import randint, uniform
from params import Params

# frequently used constants
meters_per_degree = 102.47e3
degrees_per_meter = 102.47e3 ** -1


def make_obj_list():
    objs = []
    # print "About to try to enumerate array"
    for i, o in enumerate(Params.objs_array):
        # print "In for loop"
        if o == 1:
            objs.append(i)
    # print "Objectives in list: {}".format(objs)
    Params.objs_used = objs


def update_obj_array(bitmap):
    assert len(bitmap) == len(Params.objs_array)
    for i, val in enumerate(bitmap):
        Params.objs_array[i] = val


def is_valid(cell):
    return cell.objectives[Params.obstacle_error] == 0 and \
           cell.objectives[Params.target_reward] == -1000 * len(Params.goals) and \
           cell.objectives[Params.obstacle_intrusion] == 0


def midpoint_formula(point_a, point_b):
    return [((point_a[0] + point_b[0]) / 2.0), ((point_a[1] + point_b[1]) / 2.0)]


def distance_formula(point_a, point_b):
    return math.sqrt((((point_b[0] - point_a[0]) * meters_per_degree) ** 2) +
                     (((point_b[1] - point_a[1]) * meters_per_degree) ** 2))


def distance_formula_xy(point_a, point_b):
    assert len(point_a) == 2 and len(point_b) == 2
    dist_x = (abs(point_b[1] - point_a[1])) * meters_per_degree  # lon second
    dist_y = (abs(point_b[0] - point_a[0])) * meters_per_degree  # lat first
    return dist_x, dist_y


def set_max_distance():
    start_end_dist = distance_formula(Params.start_point, Params.end_point)
    if start_end_dist <= 100:
        Params.max_dist = 200
    else:
        Params.max_dist = 2 * start_end_dist


def fix_mutation(point):
    geofence = Params.max_dist
    if distance_formula(Params.start_point, point) > geofence:
        # calculate angle
        x, y = distance_formula_xy(Params.start_point, point)
        angle = math.atan2(y, x)
        # find edge of circle keeping the same angle
        new_x = geofence * math.cos(angle)
        new_y = geofence * math.sin(angle)
        # convert these measures into a GPS point
        x_deg = new_x * degrees_per_meter
        y_deg = new_y * degrees_per_meter
        return [Params.start_point[0] + y_deg, Params.end_point[1] + x_deg]
    return point


# Takes a point p = [lat, lon] and reverses the coordinates
# so that it works in standard Cartesian coordinates (x, y)
def convert(p):
    cp = deepcopy(p)
    new_p = [cp[1], cp[0]]
    return new_p


def pts_equal(p1, p2):
    if math.fabs(p1[0] - p2[0]) < 1.e-5 and math.fabs(p1[1] - p2[1]) < 1.e-5:
        return True
    return False


def cross_product_2d(v, w):
    return (v[0] * w[1]) - (v[1] * w[0])


def interval_intersection(ab, xy):
    # b > x and a < y
    return ab[1] > xy[0] and ab[0] < xy[1]


# returns data about how two line segments intersect (or don't) and a POI
def find_intersection_point(s1p1, s1p2, s2p1, s2p2):
    """ Return value key:
        0: no segment intersection (no point)
        1: equal segments (no point)
        2: share one endpoint CROSSOVER (shared_endpoint) {s/s or e/e}
        3: share one endpoint NO CROSSOVER (shared_endpoint) {s/e or e/s}
        4: co-linear (no point)
        5: seg1 endpoint on seg2 (seg1 endpoint)
        6: seg2 endpoint on seg1 (seg2 endpoint)
        7: classical intersection (x, y)
    """
    p = s1p1
    # p + r = s1p2
    r = [s1p2[0] - p[0], s1p2[1] - p[1]]
    q = s2p1
    # q + s = s2p2
    s = [s2p2[0] - q[0], s2p2[1] - q[1]]
    # (q - p)
    qmp = [q[0] - p[0], q[1] - p[1]]
    # (r x s)
    rxs = cross_product_2d(r, s)
    # (q - p) x r
    qmpxr = cross_product_2d(qmp, r)

    # print "p: {}".format(p)
    # print "r: {}".format(r)
    # print "q: {}".format(q)
    # print "s: {}".format(s)
    # print "qmp: {}".format(qmp)
    # print "rxs: {}".format(rxs)
    # print "qmpxr: {}".format(qmpxr)

    if rxs == 0 and qmpxr == 0:
        # print "CASE 1"
        # segments are co-linear: (maybe intersect maybe disjoint)
        rdr = dot_product(r, r)
        rdr = rdr if rdr != 0 else 1.e-8
        t0 = dot_product(qmp, r) / rdr
        sdr = dot_product(s, r)
        t1 = (t0 + sdr) / rdr
        # check for segment intersection of [t0,t1] and [0,1] (note that if (s dot r) > 0 check [t1,t0] instead)
        if sdr > 0:
            # check [t1,t0]
            intersect = interval_intersection([t1, t0], [0, 1])
        else:
            # check [t0,t1]
            intersect = interval_intersection([t0, t1], [0, 1])
        if intersect:
            return 4, None
        else:
            if (s1p1 == s2p1 and s1p2 == s2p2) or (s1p1 == s2p2 and s1p2 == s2p1):
                return 1, None
            else:
                return 0, None
    elif rxs == 0 and qmpxr != 0:
        # print "CASE 2"
        # parallel no intersection
        return 0, None
    elif rxs != 0:
        # (q - p) x s
        qmpxs = cross_product_2d(qmp, s)
        # print "qmpxs: {}".format(qmpxs)
        t = qmpxs / rxs
        u = qmpxr / rxs
        # print "t: {}".format(t)
        # print "u: {}".format(u)
        if 0 <= t <= 1 and 0 <= u <= 1:
            # print "CASE 3"
            point = [p[0] + t * r[0], p[1] + t * r[1]]
            if t == 0:
                if u == 0:
                    return 2, point
                elif u == 1:
                    return 3, point
                else:
                    return 5, point
            elif t == 1:
                if u == 0:
                    return 3, point
                elif u == 1:
                    return 2, point
                else:
                    return 5, point
            else:
                if u == 0:
                    return 6, point
                elif u == 1:
                    return 6, point
                else:
                    return 7, point
        else:
            # print "CASE 4"
            # not parallel and not intersecting
            return 0, None


def poly_intersection(p1, p2, obstacle):
    i = 0
    while i < len(obstacle) - 3:
        type, point = find_intersection_point(p1, p2, [obstacle[i + 1], obstacle[i]],
                                              [obstacle[i + 3], obstacle[i + 2]])
        if type != 0:
            return True
        i += 2
    type, point = find_intersection_point(p1, p2, [obstacle[len(obstacle) - 1], obstacle[len(obstacle) - 2]],
                                          [obstacle[1], obstacle[0]])
    if type != 0:
        return True
    return False


def dot_product(vector1, vector2):
    assert len(vector1) == len(vector2)
    product_sum = 0
    for component in xrange(len(vector1)):
        product_sum += (vector1[component] * vector2[component])
    return product_sum


def distance_formula_gps(point_a, point_b):
    return math.sqrt(
        ((point_b[0] - point_a[0]) ** 2) + ((point_b[1] - point_a[1]) ** 2))


def rand_waypoint(max_radius, min_radius, center):
    radius = uniform(min_radius, max_radius)
    angle = randint(0, 360)

    x = radius * math.cos(math.radians(angle))
    y = radius * math.sin(math.radians(angle))

    lat_degree_offset = y * degrees_per_meter
    lon_degree_offset = x * degrees_per_meter

    waypoint_lat = round(center[0] + lat_degree_offset, 6)  # TODO ADDED ROUNDING FOR DEBUGGING
    waypoint_lon = round(center[1] + lon_degree_offset, 6)

    return [waypoint_lat, waypoint_lon]


# Given a latitude and longitude, calculate the offsets from
# Origin which is also specified in GPS coordinates.
def get_meters(lat, lon, origin):
    diff_x = lat - origin[0]
    diff_y = lon - origin[1]
    x_offset = 102470 * diff_x
    y_offset = 102470 * diff_y
    return x_offset, y_offset


def mean(data):
    """Return the sample arithmetic mean of data."""
    n = len(data)
    if n < 1:
        raise ValueError('mean requires at least one data point')
    return sum(data) / float(n)  # in Python 2 use sum(data)/float(n)


def _ss(data):
    """Return sum of square deviations of sequence data."""
    c = mean(data)
    ss = sum((x - c) ** 2 for x in data)
    return ss


def pstdev(data):
    """Calculates the population standard deviation."""
    n = len(data)
    if n < 2:
        raise ValueError('variance requires at least two data points')
    ss = _ss(data)
    pvar = ss / n  # the population variance
    return pvar ** 0.5


def quadratic_formula(a, b, c):
    assert ((b ** 2) - (4 * a * c)) >= 0
    x_plus = ((-b) + ((b ** 2) - (4 * a * c)) ** 0.5) / (2 * a)
    x_minus = ((-b) - ((b ** 2) - (4 * a * c)) ** 0.5) / (2 * a)
    return x_plus, x_minus


def line_through_circle_intersections(center_xy, dy, dx, radius):
    theta1 = math.atan(dy / dx) if dx != 0 else (math.pi / 2)
    theta2 = theta1 + math.pi

    x1 = radius * math.cos(theta1) + center_xy[0]
    y1 = radius * math.sin(theta1) + center_xy[1]
    x2 = radius * math.cos(theta2) + center_xy[0]
    y2 = radius * math.sin(theta2) + center_xy[1]

    return [x1, y1], [x2, y2]
