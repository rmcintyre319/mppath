import sys

from params import Params
from obstacle import Obstacle
import utilities


def load_input_data(input_num):
    # start_green = [28.031107, -81.945729] old
    # end_green = [28.031110, -81.945130] old
    start_green = [28.031110, -81.945729]
    end_green = [28.031107, -81.945130]
    # start_sidewalk = [28.031014, -81.946056]
    start_bench = [28.031294, -81.945775]
    origin_green = [28.030131, -81.946461]  # coordinates of lower-left corner for graphing software
    origin_alternate = [28.029900, -81.946661]  # coordinates of lower-left corner for graphing software
    end_highschool = [28.032738, -81.938785]
    end_horseshoe = [28.031257, -81.945235]
    start_branscomb = [28.030876, -81.948913]
    end_dorms = [28.030432, -81.945429]
    # end_jenkins = [28.031671, -81.945408]

    obs = []
    goals = []
    early_termination_threshold = -1  # default value
    scale_factor = 3.75  # default value

    if input_num == 1:
        # input 1: slalom (test with objectives and obstacles)
        # start green end green
        obs.append([28.031107, -81.945629, 1])
        obs.append([28.031107, -81.945529, 1])
        obs.append([28.031107, -81.945429, 1])
        obs.append([28.031107, -81.945329, 1])
        obs.append([28.031107, -81.945229, 1])

        goals.append([28.031137, -81.945629, 1])
        goals.append([28.031077, -81.945529, 1])
        goals.append([28.031137, -81.945429, 1])
        goals.append([28.031077, -81.945329, 1])
        goals.append([28.031137, -81.945229, 1])

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 2:
        # input 2
        # start/end green
        obs.append([28.031107, -81.945649, 5])
        obs.append([28.031157, -81.945479, 4])
        obs.append([28.031057, -81.945479, 4])
        obs.append([28.031157, -81.945379, 4])
        obs.append([28.031057, -81.945379, 4])
        obs.append([28.031107, -81.945210, 5])

        goals.append([28.031107, -81.945429, 2])

        early_termination_threshold = 66.75178 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 3:
        # input 3, slalom w/ goals and obstacles colinear
        obs.append([28.031107, -81.945629, 3])
        obs.append([28.031107, -81.945529, 3])
        obs.append([28.031107, -81.945429, 3])
        obs.append([28.031107, -81.945329, 3])
        obs.append([28.031107, -81.945229, 3])

        goals.append([28.031107, -81.945579, 1])
        goals.append([28.031107, -81.945479, 1])
        goals.append([28.031107, -81.945379, 1])
        goals.append([28.031107, -81.945279, 1])

        early_termination_threshold = 72.45074 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 4:
        # input 4 -- simple test - single large obstacle, centered
        obs.append([28.031107, -81.945429, 10])

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 5:
        # input 5 -- diamond radius 5
        obs.append([28.031107, -81.945575, 5])
        obs.append([28.031107, -81.945300, 5])
        obs.append([28.031253, -81.945429, 5])
        obs.append([28.030961, -81.945429, 5])

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 6:
        # input 6 -- similar to input 2 but with smaller obstacles
        #            and more spacing
        obs.append([28.031107, -81.945649, 3])
        obs.append([28.031207, -81.945499, 3])
        obs.append([28.031007, -81.945499, 3])
        obs.append([28.031207, -81.945359, 3])
        obs.append([28.031007, -81.945359, 3])
        obs.append([28.031107, -81.945210, 3])

        goals.append([28.031107, -81.945429, 2])

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 7:
        # horseshoe v2 radius 11 end_highschool
        obs.append([28.032738, -81.938352, 11])
        obs.append([28.033141, -81.938046, 28.032941, -81.938046, 28.032941, -81.938851, 28.032535, -81.938851,
                    28.032535, -81.938046, 28.032335, -81.938046, 28.032335, -81.939051, 28.033141, -81.939051,
                    28.033141, -81.938046])
        # goals.append([28.033100, -81.937870, 2])

        scale_factor = 0.75
        early_termination_threshold = 920.0 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_highschool
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 8:
        # This input starts and ends at the same point (start_green)
        obs.append([28.031107, -81.9455338209232, 10])

        goals.append([28.03121434849224, -81.9455338209232, 1])
        goals.append([28.031107, -81.94542647243095, 1])
        goals.append([28.030999651507756, -81.9455338209232, 1])

        Params.start_point = start_green
        Params.end_point = start_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 9:
        # Vinny's torture test
        obs.append([28.031184, -81.945388, 28.031184, -81.945038, 28.031030, -81.945038, 28.031030, -81.945388,
                    28.031072, -81.945388, 28.031072, -81.945078, 28.031142, -81.945078, 28.031142, -81.945388])
        obs.append([28.031107, -81.9456314104616, 3])
        obs.append([28.0312045895384, -81.9456314104616, 3])
        obs.append([28.0312045895384, -81.9455338209232, 3])
        obs.append([28.0312045895384, -81.9454362313848, 3])
        obs.append([28.031009410461596, -81.9456314104616, 3])
        obs.append([28.031009410461596, -81.9455338209232, 3])
        obs.append([28.031009410461596, -81.9454362313848, 3])
        obs.append([28.031107, -81.94545574929248, 5])
        obs.append([28.03111975895384, -81.94529960603103, 2])
        obs.append([28.031094241046158, -81.9452215344003, 2])

        early_termination_threshold = 62.85 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 10:
        # input 10, slalom w/ goals slightly off-center and
        # alternating high-low
        obs.append([28.031107, -81.945629, 3])
        obs.append([28.031107, -81.945529, 3])
        obs.append([28.031107, -81.945429, 3])
        obs.append([28.031107, -81.945329, 3])
        obs.append([28.031107, -81.945229, 3])

        goals.append([28.031117, -81.945579, 1])
        goals.append([28.031097, -81.945479, 1])
        goals.append([28.031117, -81.945379, 1])
        goals.append([28.031097, -81.945279, 1])

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 11:
        # 3 columns with offset openings for passage
        obs.append([28.030407, -81.945659, 28.031157, -81.945659, 28.031157, -81.945599, 28.030407, -81.945599])
        obs.append([28.031227, -81.945659, 28.031807, -81.945659, 28.031807, -81.945599, 28.031227, -81.945599])
        obs.append([28.030407, -81.945459, 28.030987, -81.945459, 28.030987, -81.945399, 28.030407, -81.945399])
        obs.append([28.031057, -81.945459, 28.031807, -81.945459, 28.031807, -81.945399, 28.031057, -81.945399])
        obs.append([28.030407, -81.945259, 28.031157, -81.945259, 28.031157, -81.945199, 28.030407, -81.945199])
        obs.append([28.031227, -81.945259, 28.031807, -81.945259, 28.031807, -81.945199, 28.031227, -81.945199])

        # goals.append([28.031107, -81.945529, 1])
        # goals.append([28.031107, -81.945329, 1])
        # goals.append([28.031015, -81.945429, 1])
        early_termination_threshold = 72.25 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 12:
        # test of line segment barrior detection
        obs.append([28.03121434849224, -81.9455338209232, 28.030999651507756, -81.9455338209232])
        obs.append([28.031067, -81.945329, 28.031147, -81.945329, 28.031147, -81.945229, 28.031067, -81.945229])
        obs.append([28.031197, -81.945329, 28.031277, -81.945329, 28.031277, -81.945229, 28.031197, -81.945229])
        obs.append([28.030937, -81.945329, 28.031017, -81.945329, 28.031017, -81.945229, 28.030937, -81.945229])
        obs.append([28.031107, -81.945649, 5])

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 13:
        # traditional college quad
        obs.append([28.031507, -81.946000, 10])
        obs.append([28.031597, -81.945429, 3.7])
        obs.append([28.031597, -81.945229, 4.2])
        obs.append([28.030597, -81.945729, 4.1])
        obs.append([28.030597, -81.945529, 3.5])
        obs.append([28.030597, -81.945329, 4.3])
        obs.append([28.030597, -81.945129, 3.9])
        obs.append([28.031697, -81.945829, 28.031997, -81.945829, 28.031997, -81.945159, 28.031697, -81.945159])
        obs.append([28.031717, -81.945079, 9])
        obs.append([28.030797, -81.945009, 28.031497, -81.945009, 28.031497, -81.944709, 28.030797, -81.944709])
        obs.append([28.030717, -81.944959, 12])
        obs.append([28.031397, -81.945109, 9])
        obs.append([28.030887, -81.945940, 11])

        goals.append([28.031107, -81.944609, 1])
        goals.append([28.032027, -81.945500, 1])
        goals.append([28.030507, -81.945329, 1])

        early_termination_threshold = 455.74 * Params.threshold_percent_buffer

        # early_termination_threshold = 561.62090325 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 14:
        # St Leo
        obs.append([28.337716, -82.257349, 28.337952, -82.257601, 28.338320, -82.257180, 28.338079, -82.256922])
        obs.append([28.337390, -82.257139, 28.337620, -82.257139, 28.337620, -82.256668, 28.337390, -82.256668])
        obs.append([28.336675, -82.256969, 28.336859, -82.256969, 28.336859, -82.256451, 28.336675, -82.256451])
        obs.append([28.335935, -82.256995, 28.336141, -82.256995, 28.336141, -82.256478, 28.335935, -82.256478])
        obs.append([28.335943, -82.256115, 28.336143, -82.256115, 28.336143, -82.255775, 28.335943, -82.255775])
        obs.append([28.335916, -82.257580, 28.336364, -82.257580, 28.336364, -82.257405, 28.335916, -82.257405])
        obs.append([28.337111, -82.256242, 28.337234, -82.256242, 28.337234, -82.256045, 28.337111, -82.256045])
        obs.append([28.337030, -82.255891, 28.337144, -82.255891, 28.337144, -82.255432, 28.337030, -82.255432])
        obs.append([28.337238, -82.255897, 28.337403, -82.255897, 28.337403, -82.254998, 28.337238, -82.254998])
        obs.append([28.337609, -82.256024, 28.337741, -82.256024, 28.337741, -82.255716, 28.337810, -82.255716,
                    28.337810, -82.255297, 28.337680, -82.255297, 28.337678, -82.255611, 28.337609, -82.255611])
        obs.append([28.337991, -82.256155, 28.338122, -82.256155, 28.338122, -82.255853, 28.338195, -82.255853,
                    28.338195, -82.255434, 28.338072, -82.255434, 28.338072, -82.255749, 28.337991, -82.255749])
        obs.append([28.338167, -82.256137, 28.338167, -82.256491, 28.338324, -82.256683, 28.338739, -82.256683,
                    28.338739, -82.256478, 28.338409, -82.256478, 28.338332, -82.256403, 28.338309, -82.256137])
        obs.append([28.338247, -82.256072, 28.338578, -82.256072, 28.338659, -82.256150, 28.338659, -82.256403,
                    28.338828, -82.256403, 28.338828, -82.256050, 28.338639, -82.255853, 28.338247, -82.255853])
        '''
        obs.append([28.337700, -82.256373, 7])
        obs.append([28.337032, -82.256294, 4.5])
        obs.append([28.337216, -82.257498, 5])
        obs.append([28.338484, -82.257238, 5.2])
        obs.append([28.336527, -82.257222, 15])
        '''

        scale_factor = 2.0

        early_termination_threshold = 318 * Params.threshold_percent_buffer

        Params.start_point = [28.335746, -82.257282]
        # Params.end_point = [28.338311, -82.255633]
        Params.end_point = [28.338517, -82.256386]
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = [28.335458, -82.258257]
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 15:
        # Florida Southern buildings
        obs.append([28.030930, -81.948778, 28.031119, -81.948670, 28.031017, -81.948453, 28.030769, -81.948606,
                    28.030814, -81.948721, 28.030890, -81.948689])
        obs.append([28.030508, -81.948829, 28.030617, -81.948896, 28.030710, -81.948713, 28.030610, -81.948641])
        obs.append([28.029997, -81.947646, 28.030859, -81.948230, 28.031029, -81.947919, 28.030163, -81.947329])
        obs.append([28.030196, -81.947120, 28.030286, -81.947120, 28.030286, -81.947238, 28.030589, -81.947238,
                    28.030589, -81.947013, 28.030196, -81.947013])
        obs.append([28.030745, -81.947064, 28.031238, -81.947064, 28.031238, -81.946696, 28.030745, -81.946696])
        obs.append([28.030745, -81.946482, 28.031238, -81.946482, 28.031238, -81.946154, 28.030745, -81.946154])
        obs.append([28.030156, -81.946484, 28.030471, -81.946484, 28.030471, -81.946251, 28.030260, -81.946251,
                    28.030260, -81.946184, 28.030156, -81.946184])
        obs.append([28.030511, -81.945980, 28.030662, -81.945980, 28.030662, -81.945645, 28.030511, -81.945645])
        obs.append([28.030229, -81.945894, 28.030328, -81.945921, 28.030373, -81.945634, 28.030276, -81.945613])
        obs.append([28.029398, -81.946468, 28.029864, -81.946774, 28.029978, -81.946565, 28.029514, -81.946259])
        obs.append([28.029550, -81.945578, 28.030044, -81.945905, 28.030153, -81.945707, 28.029661, -81.945377])
        obs.append([28.031440, -81.946194, 28.031900, -81.946194, 28.031900, -81.945609, 28.032020, -81.945609,
                    28.032020, -81.945037, 28.031775, -81.945037, 28.031775, -81.945541, 28.031440, -81.945541])
        '''
        obs.append([28.031100, -81.948238, 12])
        obs.append([28.031300, -81.948000, 10])
        # obs.append([28.030700, -81.946104, 9])
        # obs.append([28.030500, -81.946104, 11])
        obs.append([28.031239, -81.946040, 10])
        obs.append([28.031339, -81.946440, 8])
        obs.append([28.031239, -81.945540, 12])
        obs.append([28.030087, -81.947062, 8])
        '''

        scale_factor = 1.5

        early_termination_threshold = 360.741271228 * Params.threshold_percent_buffer

        Params.start_point = start_branscomb
        Params.end_point = end_dorms
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = [28.028600, -81.949300]
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 16:
        # input 16, slalom w/ goals and obstacles colinear
        obs.append([28.031107, -81.945729, 3])
        obs.append([28.031107, -81.945629, 3])
        obs.append([28.031107, -81.945529, 3])
        obs.append([28.031107, -81.945429, 3])
        obs.append([28.031107, -81.945329, 3])
        obs.append([28.031107, -81.945229, 3])

        goals.append([28.031107, -81.945679, 1])
        goals.append([28.031107, -81.945579, 1])
        goals.append([28.031107, -81.945479, 1])
        goals.append([28.031107, -81.945379, 1])
        goals.append([28.031107, -81.945279, 1])

        early_termination_threshold = 87.9597239258 * Params.threshold_percent_buffer

        Params.start_point = start_bench
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 17:
        # This input starts and ends at the same point (start_green)
        obs.append([28.031107, -81.945533, 10])

        goals.append([28.031217, -81.945533, 1])
        goals.append([28.031107, -81.945423, 1])
        goals.append([28.030997, -81.945533, 1])

        early_termination_threshold = 101.0 * Params.threshold_percent_buffer

        Params.start_point = start_bench
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 18:
        # Plus sign
        obs.append([28.031097, -81.945629, 28.031117, -81.945629, 28.031117, -81.945529, 28.031217, -81.945529,
                    28.031217, -81.945509, 28.031117, -81.945509, 28.031117, -81.945329, 28.031097, -81.945329,
                    28.031097, -81.945509, 28.030997, -81.945509, 28.030997, -81.945529, 28.031097, -81.945529])

        goals.append([28.031167, -81.945389, 1])
        goals.append([28.031057, -81.945389, 1])
        # goals.append([28.030997, -81.945533, 1])

        early_termination_threshold = 84.5 * Params.threshold_percent_buffer

        Params.start_point = start_bench
        Params.end_point = [28.031057, -81.945589]
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = [origin_green[0] + 0.0003, origin_green[1] + 0.0003]
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor * 1.5

        return

    elif input_num == 19:
        # horseshoe -- copy of input 7 but on the green
        obs.append([28.031257, -81.945002, 8])
        obs.append([28.031460, -81.944946, 28.031410, -81.944946, 28.031410, -81.945301, 28.031104, -81.945301,
                    28.031104, -81.944946, 28.031054, -81.944946, 28.031054, -81.945351, 28.031460, -81.945351])
        # goals.append([28.031619, -81.944862, 1])
        # goals.append([28.031257, -81.944862, 1])

        early_termination_threshold = 132.0 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_horseshoe
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 20:
        # horseshoe -- similar to input 19 but with additional obstacles
        obs.append([28.031257, -81.945002, 8])
        obs.append([28.031460, -81.944946, 28.031410, -81.944946, 28.031410, -81.945301, 28.031104, -81.945301,
                    28.031104, -81.944946, 28.031054, -81.944946, 28.031054, -81.945351, 28.031460, -81.945351])
        obs.append([28.031330, -81.945130, 3])
        obs.append([28.031184, -81.945130, 3])
        # goals.append([28.031619, -81.944862, 1])
        # goals.append([28.031257, -81.944862, 1])

        early_termination_threshold = 133.0 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_horseshoe
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 21:
        # Very similar to input 9 but with slightly wider spacing in the section containing the destination
        obs.append([28.031184, -81.945370, 28.031184, -81.945038, 28.031030, -81.945038, 28.031030, -81.945370,
                    28.031070, -81.945370, 28.031070, -81.945078, 28.031144, -81.945078, 28.031144, -81.945370])
        obs.append([28.031107, -81.9456314104616, 3])
        obs.append([28.0312045895384, -81.9456314104616, 3])
        obs.append([28.0312045895384, -81.9455338209232, 3])
        obs.append([28.0312045895384, -81.9454362313848, 3])
        obs.append([28.031009410461596, -81.9456314104616, 3])
        obs.append([28.031009410461596, -81.9455338209232, 3])
        obs.append([28.031009410461596, -81.9454362313848, 3])
        obs.append([28.031107, -81.94545574929248, 5])
        obs.append([28.03112175895384, -81.94529960603103, 2])
        obs.append([28.031092241046158, -81.9452215344003, 2])

        early_termination_threshold = 62.85 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 22:
        # Lake Claire Apartments - UCF
        # Lower left grouping
        obs.append([28.030156, -81.948548, 28.030336, -81.948548, 28.030336, -81.948238, 28.030156, -81.948238])
        obs.append([28.030246, -81.948778, 28.030556, -81.948778, 28.030556, -81.948598, 28.030246, -81.948598])
        obs.append([28.030626, -81.948738, 28.030806, -81.948738, 28.030806, -81.948428, 28.030626, -81.948428])
        obs.append([28.030596, -81.948328, 28.030776, -81.948328, 28.030776, -81.948018, 28.030596, -81.948018])
        obs.append([28.030206, -81.948118, 28.030516, -81.948118, 28.030516, -81.947938, 28.030206, -81.947938])

        # Middle grouping
        obs.append([28.030756, -81.947788, 28.030936, -81.947788, 28.030936, -81.947478, 28.030756, -81.947478])
        obs.append([28.030846, -81.948018, 28.031156, -81.948018, 28.031156, -81.947838, 28.030846, -81.947838])
        obs.append([28.031226, -81.947978, 28.031406, -81.947978, 28.031406, -81.947668, 28.031226, -81.947668])
        obs.append([28.031196, -81.947568, 28.031376, -81.947568, 28.031376, -81.947258, 28.031196, -81.947258])
        obs.append([28.030806, -81.947358, 28.031116, -81.947358, 28.031116, -81.947178, 28.030806, -81.947178])

        # Upper right grouping
        obs.append([28.031406, -81.946938, 28.031586, -81.946938, 28.031586, -81.946628, 28.031406, -81.946628])
        obs.append([28.031496, -81.947168, 28.031806, -81.947168, 28.031806, -81.946988, 28.031496, -81.946988])
        obs.append([28.031876, -81.947128, 28.032056, -81.947128, 28.032056, -81.946818, 28.031876, -81.946818])
        obs.append([28.031846, -81.946718, 28.032026, -81.946718, 28.032026, -81.946408, 28.031846, -81.946408])
        obs.append([28.031456, -81.946508, 28.031766, -81.946508, 28.031766, -81.946328, 28.031456, -81.946328])

        # Trees
        # obs.append([28.031000, -81.947000, 9])
        # obs.append([28.030626, -81.947500, 10])
        # obs.append([28.031456, -81.948100, 10])

        scale_factor = 1.75

        # Use 200 to force path through middle group of buildings
        early_termination_threshold = 197.33 * Params.threshold_percent_buffer

        Params.start_point = [28.031620, -81.946800]
        Params.end_point = [28.030400, -81.948200]
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = [28.028800, -81.949750]
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 23:
        # traditional college quad
        # obs.append([28.031507, -81.946000, 10])
        obs.append([28.030700, -81.945829, 28.030800, -81.945829, 28.030800, -81.945129, 28.030700, -81.945129])
        obs.append([28.031697, -81.945829, 28.031997, -81.945829, 28.031997, -81.945159, 28.031697, -81.945159])
        obs.append([28.030797, -81.945009, 28.031497, -81.945009, 28.031497, -81.944709, 28.030797, -81.944709])

        goals.append([28.031107, -81.944609, 1])
        goals.append([28.032027, -81.945500, 1])
        goals.append([28.030507, -81.945329, 1])

        scale_factor = 1.75

        early_termination_threshold = 445.74 * Params.threshold_percent_buffer

        # early_termination_threshold = 561.62090325 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        # Params.origin = origin_green
        Params.origin = [28.029831, -81.946261]
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 24:
        # Vinny's torture test -- similar to input 9 but with circles replaced with rectangles
        # Circles that were abutting horseshoe were incorporated into horseshoe
        # horseshoe, defined from upper-left corner
        obs.append([28.031184, -81.945388, 28.031184, -81.945038, 28.031030, -81.945038, 28.031030, -81.945388,
                    28.031072, -81.945388, 28.031072, -81.945211, 28.031112, -81.945211, 28.031112, -81.945171,
                    28.031072, -81.945171, 28.031072, -81.945078, 28.031142, -81.945078, 28.031142, -81.945279,
                    28.031102, -81.945279, 28.031102, -81.945319, 28.031142, -81.945319, 28.031142, -81.945388])

        # left center
        obs.append([28.031077, -81.945661, 28.031137, -81.945661, 28.031137, -81.945601, 28.031077, -81.945601])
        # top row
        obs.append([28.031174, -81.945661, 28.031234, -81.945661, 28.031234, -81.945601, 28.031174, -81.945601])
        obs.append([28.031174, -81.945564, 28.031234, -81.945564, 28.031234, -81.945504, 28.031174, -81.945504])
        obs.append([28.031174, -81.945466, 28.031234, -81.945466, 28.031234, -81.945406, 28.031174, -81.945406])
        # bottom row
        obs.append([28.030979, -81.945661, 28.031039, -81.945661, 28.031039, -81.945601, 28.030979, -81.945601])
        obs.append([28.030979, -81.945564, 28.031039, -81.945564, 28.031039, -81.945504, 28.030979, -81.945504])
        obs.append([28.030979, -81.945466, 28.031039, -81.945466, 28.031039, -81.945406, 28.030979, -81.945406])
        # large center
        obs.append([28.031062, -81.945520, 28.031152, -81.945520, 28.031152, -81.945430, 28.031062, -81.945430])

        early_termination_threshold = 62.85 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 25:
        # Nested horseshoes

        # left grouping
        # outermost horseshoe
        obs.append([28.030950, -81.945900, 28.030960, -81.945900, 28.030960, -81.945500, 28.031260, -81.945500,
                    28.031260, -81.945900, 28.031270, -81.945900, 28.031270, -81.945490, 28.030950, -81.945490])
        '''
        obs.append([28.030920, -81.945900, 28.030960, -81.945900, 28.030960, -81.945500, 28.031260, -81.945500,
                28.031260, -81.945900, 28.031270, -81.945900, 28.031270, -81.945490, 28.030950, -81.945490,
                28.030950, -81.945890, 28.030920, -81.945890])
        '''
        # middle horseshoe
        obs.append([28.031010, -81.945900, 28.031210, -81.945900, 28.031210, -81.945550, 28.031200, -81.945550,
                    28.031200, -81.945890, 28.031020, -81.945890, 28.031020, -81.945550, 28.031010, -81.945550])

        # innermost horseshoe
        obs.append([28.031050, -81.945840, 28.031060, -81.945840, 28.031060, -81.945560, 28.031160, -81.945560,
                    28.031160, -81.945840, 28.031170, -81.945840, 28.031170, -81.945550, 28.031050, -81.945550])

        # right grouping
        # outermost horseshoe

        obs.append([28.030950, -81.945340, 28.031270, -81.945340, 28.031270, -81.944930, 28.031260, -81.944930,
                    28.031260, -81.945330, 28.030960, -81.945330, 28.030960, -81.944930, 28.030950, -81.944930])
        '''

        obs.append([28.030950, -81.945340, 28.031270, -81.945340, 28.031270, -81.944930, 28.031260, -81.944930,
                28.031260, -81.945330, 28.030960, -81.945330, 28.030960, -81.944930, 28.030920, -81.944930,
                28.030920, -81.944940, 28.030950, -81.944940])
        '''
        # middle horseshoe
        obs.append([28.031010, -81.945280, 28.031020, -81.945280, 28.031020, -81.944940, 28.031200, -81.944940,
                    28.031200, -81.945280, 28.031210, -81.945280, 28.031210, -81.944930, 28.031010, -81.944930])

        # innermost horseshoe
        obs.append([28.031050, -81.945280, 28.031170, -81.945280, 28.031170, -81.944990, 28.031160, -81.944990,
                    28.031160, -81.945270, 28.031060, -81.945270, 28.031060, -81.944990, 28.031050, -81.944990])

        # verticals
        # obs.append([28.031230, -81.945420, 28.031410, -81.945420, 28.031410, -81.945410, 28.031230, -81.945410])
        # obs.append([28.030810, -81.945420, 28.030990, -81.945420, 28.030990, -81.945410, 28.030810, -81.945410])
        # obs.append([28.031260, -81.945420, 28.031410, -81.945420, 28.031410, -81.945410, 28.031260, -81.945410])
        # obs.append([28.030810, -81.945420, 28.030960, -81.945420, 28.030960, -81.945410, 28.030810, -81.945410])
        obs.append([28.031230, -81.945420, 28.031710, -81.945420, 28.031710, -81.945410, 28.031230, -81.945410])
        obs.append([28.030510, -81.945420, 28.030990, -81.945420, 28.030990, -81.945410, 28.030510, -81.945410])

        # goals.append([28.031050, -81.945415, 1])

        early_termination_threshold = 315 * Params.threshold_percent_buffer
        # start = [28.031110, -81.945729]
        # end = [28.031110, -81.945130]
        start = [28.031080, -81.945729]
        end = [28.031140, -81.945130]
        # start = [28.030900, -81.945900]
        # end = [28.030900, -81.944930]
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 26:
        # Vertical Nested horseshoes

        # bottom grouping
        # outermost horseshoe
        obs.append([28.030610, -81.945595, 28.031010, -81.945595, 28.031010, -81.945245, 28.030610, -81.945245,
                    28.030610, -81.945255, 28.031000, -81.945255, 28.031000, -81.945585, 28.030610, -81.945585])
        # middle horseshoe
        obs.append([28.030610, -81.945526, 28.030941, -81.945526, 28.030941, -81.945516, 28.030620, -81.945516,
                    28.030620, -81.945316, 28.030941, -81.945316, 28.030941, -81.945306, 28.030610, -81.945306])
        # innermost horseshoe
        obs.append([28.030670, -81.945476, 28.030941, -81.945476, 28.030941, -81.945356, 28.030670, -81.945356,
                    28.030670, -81.945366, 28.030931, -81.945366, 28.030931, -81.945466, 28.030670, -81.945466])

        # top grouping
        # outermost horseshoe
        obs.append([28.031160, -81.945595, 28.031560, -81.945595, 28.031560, -81.945585, 28.031170, -81.945585,
                    28.031170, -81.945255, 28.031560, -81.945255, 28.031560, -81.945245, 28.031160, -81.945245])
        # middle horseshoe
        obs.append([28.031219, -81.945526, 28.031560, -81.945526, 28.031560, -81.945306, 28.031219, -81.945306,
                    28.031219, -81.945316, 28.031550, -81.945316, 28.031550, -81.945516, 28.031219, -81.945516])
        # innermost horseshoe
        obs.append([28.031219, -81.945476, 28.031500, -81.945476, 28.031500, -81.945466, 28.031229, -81.945466,
                    28.031229, -81.945366, 28.031500, -81.945366, 28.031500, -81.945356, 28.031219, -81.945356])

        # verticals
        # obs.append([28.031230, -81.945420, 28.031410, -81.945420, 28.031410, -81.945410, 28.031230, -81.945410])
        # obs.append([28.030810, -81.945420, 28.030990, -81.945420, 28.030990, -81.945410, 28.030810, -81.945410])

        # obs.append([28.031260, -81.945420, 28.031410, -81.945420, 28.031410, -81.945410, 28.031260, -81.945410])
        # obs.append([28.030810, -81.945420, 28.030960, -81.945420, 28.030960, -81.945410, 28.030810, -81.945410])

        # obs.append([28.031230, -81.945420, 28.031710, -81.945420, 28.031710, -81.945410, 28.031230, -81.945410])
        # obs.append([28.030510, -81.945420, 28.030990, -81.945420, 28.030990, -81.945410, 28.030510, -81.945410])

        # goals.append([28.031050, -81.945415, 1])

        early_termination_threshold = 290 * Params.threshold_percent_buffer
        end = [28.030891, -81.945416]
        start = [28.031279, -81.945416]
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 27:
        # similar to input 11 but with shorter columns
        # 3 columns with offset openings for passage
        obs.append([28.030607, -81.945659, 28.031157, -81.945659, 28.031157, -81.945599, 28.030607, -81.945599])
        obs.append([28.031227, -81.945659, 28.031607, -81.945659, 28.031607, -81.945599, 28.031227, -81.945599])
        obs.append([28.030607, -81.945459, 28.030987, -81.945459, 28.030987, -81.945399, 28.030607, -81.945399])
        obs.append([28.031057, -81.945459, 28.031607, -81.945459, 28.031607, -81.945399, 28.031057, -81.945399])
        obs.append([28.030607, -81.945259, 28.031157, -81.945259, 28.031157, -81.945199, 28.030607, -81.945199])
        obs.append([28.031227, -81.945259, 28.031607, -81.945259, 28.031607, -81.945199, 28.031227, -81.945199])

        # goals.append([28.031107, -81.945529, 1])
        # goals.append([28.031107, -81.945329, 1])
        # goals.append([28.031015, -81.945429, 1])
        early_termination_threshold = 72.25 * Params.threshold_percent_buffer

        Params.start_point = start_green
        Params.end_point = end_green
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 28:
        # similar to input 11 but with 7 columns rather than 3
        # greater variation in positions of openings
        obs.append([28.030407, -81.946059, 28.031157, -81.946059, 28.031157, -81.945999, 28.030407, -81.945999])
        obs.append([28.031227, -81.946059, 28.031807, -81.946059, 28.031807, -81.945999, 28.031227, -81.945999])

        obs.append([28.030407, -81.945859, 28.031357, -81.945859, 28.031357, -81.945799, 28.030407, -81.945799])
        obs.append([28.031427, -81.945859, 28.031807, -81.945859, 28.031807, -81.945799, 28.031427, -81.945799])

        obs.append([28.030407, -81.945659, 28.031157, -81.945659, 28.031157, -81.945599, 28.030407, -81.945599])
        obs.append([28.031227, -81.945659, 28.031807, -81.945659, 28.031807, -81.945599, 28.031227, -81.945599])

        obs.append([28.030407, -81.945459, 28.030987, -81.945459, 28.030987, -81.945399, 28.030407, -81.945399])
        obs.append([28.031057, -81.945459, 28.031807, -81.945459, 28.031807, -81.945399, 28.031057, -81.945399])

        obs.append([28.030407, -81.945259, 28.031157, -81.945259, 28.031157, -81.945199, 28.030407, -81.945199])
        obs.append([28.031227, -81.945259, 28.031807, -81.945259, 28.031807, -81.945199, 28.031227, -81.945199])

        obs.append([28.030407, -81.945059, 28.031357, -81.945059, 28.031357, -81.944999, 28.030407, -81.944999])
        obs.append([28.031427, -81.945059, 28.031807, -81.945059, 28.031807, -81.944999, 28.031427, -81.944999])

        obs.append([28.030407, -81.944859, 28.031157, -81.944859, 28.031157, -81.944799, 28.030407, -81.944799])
        obs.append([28.031227, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.031227, -81.944799])

        early_termination_threshold = 188 * Params.threshold_percent_buffer

        Params.end_point = [28.031110, -81.946129]
        Params.start_point = [28.031107, -81.944730]
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 29:
        # similar to input 11 but with 7 columns rather than 3
        obs.append([28.030407, -81.946059, 28.031157, -81.946059, 28.031157, -81.945999, 28.030407, -81.945999])
        obs.append([28.031227, -81.946059, 28.031807, -81.946059, 28.031807, -81.945999, 28.031227, -81.945999])

        obs.append([28.030407, -81.945859, 28.030957, -81.945859, 28.030957, -81.945799, 28.030407, -81.945799])
        obs.append([28.031027, -81.945859, 28.031807, -81.945859, 28.031807, -81.945799, 28.031027, -81.945799])

        obs.append([28.030407, -81.945659, 28.031157, -81.945659, 28.031157, -81.945599, 28.030407, -81.945599])
        obs.append([28.031227, -81.945659, 28.031807, -81.945659, 28.031807, -81.945599, 28.031227, -81.945599])

        obs.append([28.030407, -81.945459, 28.030987, -81.945459, 28.030987, -81.945399, 28.030407, -81.945399])
        obs.append([28.031057, -81.945459, 28.031807, -81.945459, 28.031807, -81.945399, 28.031057, -81.945399])

        obs.append([28.030407, -81.945259, 28.031157, -81.945259, 28.031157, -81.945199, 28.030407, -81.945199])
        obs.append([28.031227, -81.945259, 28.031807, -81.945259, 28.031807, -81.945199, 28.031227, -81.945199])

        obs.append([28.030407, -81.945059, 28.030957, -81.945059, 28.030957, -81.944999, 28.030407, -81.944999])
        obs.append([28.031027, -81.945059, 28.031807, -81.945059, 28.031807, -81.944999, 28.031027, -81.944999])

        obs.append([28.030407, -81.944859, 28.031157, -81.944859, 28.031157, -81.944799, 28.030407, -81.944799])
        obs.append([28.031227, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.031227, -81.944799])

        early_termination_threshold = 185 * Params.threshold_percent_buffer

        Params.start_point = [28.031107, -81.946129]
        Params.end_point = [28.031107, -81.944730]

        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals

        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 30:
        # similar to input 29 but with 2nd, shorter valid path
        obs.append([28.030407, -81.946059, 28.030737, -81.946059, 28.030737, -81.945999, 28.030407, -81.945999])
        obs.append([28.030787, -81.946059, 28.031157, -81.946059, 28.031157, -81.945999, 28.030787, -81.945999])
        obs.append([28.031227, -81.946059, 28.031807, -81.946059, 28.031807, -81.945999, 28.031227, -81.945999])
        # obs.append([28.031277, -81.946059, 28.031807, -81.946059, 28.031807, -81.945999, 28.031277, -81.945999])

        obs.append([28.030407, -81.945859, 28.030807, -81.945859, 28.030807, -81.945799, 28.030407, -81.945799])
        obs.append([28.030857, -81.945859, 28.030957, -81.945859, 28.030957, -81.945799, 28.030857, -81.945799])
        obs.append([28.031027, -81.945859, 28.031807, -81.945859, 28.031807, -81.945799, 28.031027, -81.945799])

        obs.append([28.030407, -81.945659, 28.030737, -81.945659, 28.030737, -81.945599, 28.030407, -81.945599])
        # obs.append([28.030787, -81.945659, 28.031157, -81.945659, 28.031157, -81.945599, 28.030787, -81.945599])
        # obs.append([28.031227, -81.945659, 28.031807, -81.945659, 28.031807, -81.945599, 28.031227, -81.945599])
        obs.append([28.030787, -81.945659, 28.031207, -81.945659, 28.031207, -81.945599, 28.030787, -81.945599])
        obs.append([28.031277, -81.945659, 28.031807, -81.945659, 28.031807, -81.945599, 28.031277, -81.945599])

        obs.append([28.030407, -81.945459, 28.030807, -81.945459, 28.030807, -81.945399, 28.030407, -81.945399])
        obs.append([28.030857, -81.945459, 28.030957, -81.945459, 28.030957, -81.945399, 28.030857, -81.945399])
        obs.append([28.031057, -81.945459, 28.031807, -81.945459, 28.031807, -81.945399, 28.031057, -81.945399])

        obs.append([28.030407, -81.945259, 28.030737, -81.945259, 28.030737, -81.945199, 28.030407, -81.945199])
        # obs.append([28.030787, -81.945259, 28.031157, -81.945259, 28.031157, -81.945199, 28.030787, -81.945199])
        # obs.append([28.031227, -81.945259, 28.031807, -81.945259, 28.031807, -81.945199, 28.031227, -81.945199])
        obs.append([28.030787, -81.945259, 28.031207, -81.945259, 28.031207, -81.945199, 28.030787, -81.945199])
        obs.append([28.031277, -81.945259, 28.031807, -81.945259, 28.031807, -81.945199, 28.031277, -81.945199])

        obs.append([28.030407, -81.945059, 28.030807, -81.945059, 28.030807, -81.944999, 28.030407, -81.944999])
        obs.append([28.030857, -81.945059, 28.030957, -81.945059, 28.030957, -81.944999, 28.030857, -81.944999])
        obs.append([28.031027, -81.945059, 28.031807, -81.945059, 28.031807, -81.944999, 28.031027, -81.944999])

        obs.append([28.030407, -81.944859, 28.030737, -81.944859, 28.030737, -81.944799, 28.030407, -81.944799])
        obs.append([28.030787, -81.944859, 28.031157, -81.944859, 28.031157, -81.944799, 28.030787, -81.944799])
        obs.append([28.031227, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.031227, -81.944799])
        # obs.append([28.031277, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.031277, -81.944799])

        early_termination_threshold = 170 * Params.threshold_percent_buffer

        Params.start_point = [28.031107, -81.946129]
        Params.end_point = [28.031107, -81.944730]
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 31:
        # similar to input 29 but with 2nd, shorter valid path
        # original openings on columns 1, 3, 5, 7 moved up 8 meters
        obs.append([28.030407, -81.946059, 28.030737, -81.946059, 28.030737, -81.945999, 28.030407, -81.945999])
        obs.append([28.030777, -81.946059, 28.031247, -81.946059, 28.031247, -81.945999, 28.030777, -81.945999])
        obs.append([28.031287, -81.946059, 28.031807, -81.946059, 28.031807, -81.945999, 28.031287, -81.945999])
        # obs.append([28.030787, -81.946059, 28.031807, -81.946059, 28.031807, -81.945999, 28.030787, -81.945999])

        obs.append([28.030407, -81.945859, 28.030817, -81.945859, 28.030817, -81.945799, 28.030407, -81.945799])
        obs.append([28.030857, -81.945859, 28.030957, -81.945859, 28.030957, -81.945799, 28.030857, -81.945799])
        obs.append([28.031027, -81.945859, 28.031807, -81.945859, 28.031807, -81.945799, 28.031027, -81.945799])

        obs.append([28.030407, -81.945659, 28.030737, -81.945659, 28.030737, -81.945599, 28.030407, -81.945599])
        obs.append([28.030777, -81.945659, 28.031247, -81.945659, 28.031247, -81.945599, 28.030777, -81.945599])
        obs.append([28.031287, -81.945659, 28.031807, -81.945659, 28.031807, -81.945599, 28.031287, -81.945599])

        obs.append([28.030407, -81.945459, 28.030817, -81.945459, 28.030817, -81.945399, 28.030407, -81.945399])
        obs.append([28.030857, -81.945459, 28.030957, -81.945459, 28.030957, -81.945399, 28.030857, -81.945399])
        obs.append([28.031057, -81.945459, 28.031807, -81.945459, 28.031807, -81.945399, 28.031057, -81.945399])

        obs.append([28.030407, -81.945259, 28.030737, -81.945259, 28.030737, -81.945199, 28.030407, -81.945199])
        obs.append([28.030777, -81.945259, 28.031247, -81.945259, 28.031247, -81.945199, 28.030777, -81.945199])
        obs.append([28.031287, -81.945259, 28.031807, -81.945259, 28.031807, -81.945199, 28.031287, -81.945199])

        obs.append([28.030407, -81.945059, 28.030817, -81.945059, 28.030817, -81.944999, 28.030407, -81.944999])
        obs.append([28.030857, -81.945059, 28.030957, -81.945059, 28.030957, -81.944999, 28.030857, -81.944999])
        obs.append([28.031027, -81.945059, 28.031807, -81.945059, 28.031807, -81.944999, 28.031027, -81.944999])

        obs.append([28.030407, -81.944859, 28.030737, -81.944859, 28.030737, -81.944799, 28.030407, -81.944799])
        obs.append([28.030777, -81.944859, 28.031247, -81.944859, 28.031247, -81.944799, 28.030777, -81.944799])
        obs.append([28.031287, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.031287, -81.944799])
        # obs.append([28.030787, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.030787, -81.944799])

        # early_termination_threshold = 203.23 * Params.threshold_percent_buffer
        early_termination_threshold = 205.23 * 1.01

        Params.start_point = [28.031107, -81.946129]
        Params.end_point = [28.031107, -81.944730]
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 32:
        # similar to input 29 but with 2nd and 3rd shorter valid paths
        # original openings on columns 1, 3, 5, 7 moved up 8 meters
        obs.append([28.030407, -81.946059, 28.030737, -81.946059, 28.030737, -81.945999, 28.030407, -81.945999])
        obs.append([28.030777, -81.946059, 28.031237, -81.946059, 28.031237, -81.945999, 28.030777, -81.945999])
        obs.append([28.031287, -81.946059, 28.031807, -81.946059, 28.031807, -81.945999, 28.031287, -81.945999])
        # obs.append([28.030787, -81.946059, 28.031807, -81.946059, 28.031807, -81.945999, 28.030787, -81.945999])

        obs.append([28.030407, -81.945859, 28.030677, -81.945859, 28.030677, -81.945799, 28.030407, -81.945799])
        obs.append([28.030717, -81.945859, 28.030817, -81.945859, 28.030817, -81.945799, 28.030717, -81.945799])
        obs.append([28.030857, -81.945859, 28.030957, -81.945859, 28.030957, -81.945799, 28.030857, -81.945799])
        obs.append([28.031027, -81.945859, 28.031807, -81.945859, 28.031807, -81.945799, 28.031027, -81.945799])

        obs.append([28.030407, -81.945659, 28.030677, -81.945659, 28.030677, -81.945599, 28.030407, -81.945599])
        obs.append([28.030717, -81.945659, 28.030737, -81.945659, 28.030737, -81.945599, 28.030717, -81.945599])
        obs.append([28.030777, -81.945659, 28.031237, -81.945659, 28.031237, -81.945599, 28.030777, -81.945599])
        obs.append([28.031287, -81.945659, 28.031807, -81.945659, 28.031807, -81.945599, 28.031287, -81.945599])

        obs.append([28.030407, -81.945459, 28.030677, -81.945459, 28.030677, -81.945399, 28.030407, -81.945399])
        obs.append([28.030717, -81.945459, 28.030817, -81.945459, 28.030817, -81.945399, 28.030717, -81.945399])
        obs.append([28.030857, -81.945459, 28.030957, -81.945459, 28.030957, -81.945399, 28.030857, -81.945399])
        obs.append([28.031057, -81.945459, 28.031807, -81.945459, 28.031807, -81.945399, 28.031057, -81.945399])

        obs.append([28.030407, -81.945259, 28.030677, -81.945259, 28.030677, -81.945199, 28.030407, -81.945199])
        obs.append([28.030717, -81.945259, 28.030737, -81.945259, 28.030737, -81.945199, 28.030717, -81.945199])
        obs.append([28.030777, -81.945259, 28.031237, -81.945259, 28.031237, -81.945199, 28.030777, -81.945199])
        obs.append([28.031287, -81.945259, 28.031807, -81.945259, 28.031807, -81.945199, 28.031287, -81.945199])

        obs.append([28.030407, -81.945059, 28.030677, -81.945059, 28.030677, -81.944999, 28.030407, -81.944999])
        obs.append([28.030717, -81.945059, 28.030817, -81.945059, 28.030817, -81.944999, 28.030717, -81.944999])
        obs.append([28.030857, -81.945059, 28.030957, -81.945059, 28.030957, -81.944999, 28.030857, -81.944999])
        obs.append([28.031027, -81.945059, 28.031807, -81.945059, 28.031807, -81.944999, 28.031027, -81.944999])

        obs.append([28.030407, -81.944859, 28.030737, -81.944859, 28.030737, -81.944799, 28.030407, -81.944799])
        obs.append([28.030777, -81.944859, 28.031237, -81.944859, 28.031237, -81.944799, 28.030777, -81.944799])
        obs.append([28.031287, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.031287, -81.944799])
        # obs.append([28.030787, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.030787, -81.944799])

        # early_termination_threshold = 200.4 * Params.threshold_percent_buffer
        early_termination_threshold = 200.4 * 1.01

        Params.start_point = [28.031107, -81.946129]
        Params.end_point = [28.031107, -81.944730]
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 33:
        # similar to input 29 but with 2nd, shorter valid path below and even shorter path above
        # original openings on columns 1, 3, 5, 7 moved up 8 meters

        # each group is 1 of 7 columns, each entry is an obstacle starting on the bottom of the column
        obs.append([28.030407, -81.946059, 28.030737, -81.946059, 28.030737, -81.945999, 28.030407, -81.945999])
        obs.append([28.030777, -81.946059, 28.031247, -81.946059, 28.031247, -81.945999, 28.030777, -81.945999])
        obs.append([28.031287, -81.946059, 28.031367, -81.946059, 28.031367, -81.945999, 28.031287, -81.945999])
        obs.append([28.031417, -81.946059, 28.031807, -81.946059, 28.031807, -81.945999, 28.031417, -81.945999])

        obs.append([28.030407, -81.945859, 28.030817, -81.945859, 28.030817, -81.945799, 28.030407, -81.945799])
        obs.append([28.030857, -81.945859, 28.030957, -81.945859, 28.030957, -81.945799, 28.030857, -81.945799])
        obs.append([28.031027, -81.945859, 28.031367, -81.945859, 28.031367, -81.945799, 28.031027, -81.945799])
        obs.append([28.031417, -81.945859, 28.031807, -81.945859, 28.031807, -81.945799, 28.031417, -81.945799])

        obs.append([28.030407, -81.945659, 28.030737, -81.945659, 28.030737, -81.945599, 28.030407, -81.945599])
        obs.append([28.030777, -81.945659, 28.031247, -81.945659, 28.031247, -81.945599, 28.030777, -81.945599])
        obs.append([28.031287, -81.945659, 28.031367, -81.945659, 28.031367, -81.945599, 28.031287, -81.945599])
        obs.append([28.031417, -81.945659, 28.031807, -81.945659, 28.031807, -81.945599, 28.031417, -81.945599])

        obs.append([28.030407, -81.945459, 28.030817, -81.945459, 28.030817, -81.945399, 28.030407, -81.945399])
        obs.append([28.030857, -81.945459, 28.030957, -81.945459, 28.030957, -81.945399, 28.030857, -81.945399])
        obs.append([28.031057, -81.945459, 28.031367, -81.945459, 28.031367, -81.945399, 28.031057, -81.945399])
        obs.append([28.031417, -81.945459, 28.031807, -81.945459, 28.031807, -81.945399, 28.031417, -81.945399])

        obs.append([28.030407, -81.945259, 28.030737, -81.945259, 28.030737, -81.945199, 28.030407, -81.945199])
        obs.append([28.030777, -81.945259, 28.031247, -81.945259, 28.031247, -81.945199, 28.030777, -81.945199])
        obs.append([28.031287, -81.945259, 28.031367, -81.945259, 28.031367, -81.945199, 28.031287, -81.945199])
        obs.append([28.031417, -81.945259, 28.031807, -81.945259, 28.031807, -81.945199, 28.031417, -81.945199])

        obs.append([28.030407, -81.945059, 28.030817, -81.945059, 28.030817, -81.944999, 28.030407, -81.944999])
        obs.append([28.030857, -81.945059, 28.030957, -81.945059, 28.030957, -81.944999, 28.030857, -81.944999])
        obs.append([28.031027, -81.945059, 28.031367, -81.945059, 28.031367, -81.944999, 28.031027, -81.944999])
        obs.append([28.031417, -81.945059, 28.031807, -81.945059, 28.031807, -81.944999, 28.031417, -81.944999])

        obs.append([28.030407, -81.944859, 28.030737, -81.944859, 28.030737, -81.944799, 28.030407, -81.944799])
        obs.append([28.030777, -81.944859, 28.031247, -81.944859, 28.031247, -81.944799, 28.030777, -81.944799])
        obs.append([28.031287, -81.944859, 28.031367, -81.944859, 28.031367, -81.944799, 28.031287, -81.944799])
        obs.append([28.031417, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.031417, -81.944799])
        # obs.append([28.030787, -81.944859, 28.031807, -81.944859, 28.031807, -81.944799, 28.030787, -81.944799])

        # early_termination_threshold = 170 * Params.threshold_percent_buffer
        early_termination_threshold = 170 * 1.01

        Params.start_point = [28.031107, -81.946129]
        Params.end_point = [28.031107, -81.944730]
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return

    elif input_num == 34:
        # Variant of input 25 with obstacles 2m thick (rather than 1m)
        # Nested horseshoes

        # left grouping
        # outermost horseshoe
        obs.append([28.030930, -81.945900, 28.030950, -81.945900, 28.030950, -81.945500, 28.031270, -81.945500,
                    28.031270, -81.945900, 28.031290, -81.945900, 28.031290, -81.945480, 28.030930, -81.945480])

        # middle horseshoe
        obs.append([28.031000, -81.945910, 28.031220, -81.945910, 28.031220, -81.945550, 28.031200, -81.945550,
                    28.031200, -81.945890, 28.031020, -81.945890, 28.031020, -81.945550, 28.031000, -81.945550])

        # innermost horseshoe
        obs.append([28.031050, -81.945840, 28.031070, -81.945840, 28.031070, -81.945570, 28.031150, -81.945570,
                    28.031150, -81.945840, 28.031170, -81.945840, 28.031170, -81.945550, 28.031050, -81.945550])

        # right grouping
        # outermost horseshoe
        obs.append([28.030930, -81.945350, 28.031290, -81.945350, 28.031290, -81.944930, 28.031270, -81.944930,
                    28.031270, -81.945330, 28.030950, -81.945330, 28.030950, -81.944930, 28.030930, -81.944930])

        # middle horseshoe
        obs.append([28.031000, -81.945280, 28.031020, -81.945280, 28.031020, -81.944940, 28.031200, -81.944940,
                    28.031200, -81.945280, 28.031220, -81.945280, 28.031220, -81.944920, 28.031000, -81.944920])

        # innermost horseshoe
        obs.append([28.031050, -81.945280, 28.031170, -81.945280, 28.031170, -81.944990, 28.031150, -81.944990,
                    28.031150, -81.945260, 28.031070, -81.945260, 28.031070, -81.944990, 28.031050, -81.944990])

        # verticals
        # obs.append([28.031230, -81.945420, 28.031410, -81.945420, 28.031410, -81.945410, 28.031230, -81.945410])
        # obs.append([28.030810, -81.945420, 28.030990, -81.945420, 28.030990, -81.945410, 28.030810, -81.945410])

        # obs.append([28.031260, -81.945420, 28.031410, -81.945420, 28.031410, -81.945410, 28.031260, -81.945410])
        # obs.append([28.030810, -81.945420, 28.030960, -81.945420, 28.030960, -81.945410, 28.030810, -81.945410])

        obs.append([28.031230, -81.945425, 28.031710, -81.945425, 28.031710, -81.945405, 28.031230, -81.945405])
        obs.append([28.030510, -81.945425, 28.030990, -81.945425, 28.030990, -81.945405, 28.030510, -81.945405])

        # goals.append([28.031050, -81.945415, 1])

        early_termination_threshold = 288 * Params.threshold_percent_buffer

        start = [28.031110, -81.945729]
        end = [28.031110, -81.945130]

        # start = [28.031090, -81.945729]
        # end = [28.031130, -81.945130]

        # start = [28.030900, -81.945900]
        # end = [28.030900, -81.944930]

        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

        return
    # this input was crafted from scratch to emulate input 33, but achieve the proper geometric ratios between paths
    elif input_num == 35:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        #col 1
        obs.append(
            [s[0] + 9 * unit, s[1] + 1 * unit,
             s[0] + 27 * unit, s[1] + 1 * unit,
             s[0] + 27 * unit, s[1] + 2 * unit,
             s[0] + 9 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 1 * unit,
             s[0] + 8 * unit, s[1] + 1 * unit,
             s[0] + 8 * unit, s[1] + 2 * unit,
             s[0] + 7 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 2 * unit,
             s[0] - 8 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 27 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 2 * unit,
             s[0] - 27 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 9 * unit, s[1] + 4 * unit,
             s[0] + 27 * unit, s[1] + 4 * unit,
             s[0] + 27 * unit, s[1] + 5 * unit,
             s[0] + 9 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 2 * unit, s[1] + 4 * unit,
             s[0] + 8 * unit, s[1] + 4 * unit,
             s[0] + 8 * unit, s[1] + 5 * unit,
             s[0] - 2 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 4 * unit,
             s[0] - 3 * unit, s[1] + 4 * unit,
             s[0] - 3 * unit, s[1] + 5 * unit,
             s[0] - 4 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 5 * unit, s[1] + 4 * unit,
             s[0] - 27 * unit, s[1] + 4 * unit,
             s[0] - 27 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] + 9 * unit, s[1] + 7 * unit,
             s[0] + 27 * unit, s[1] + 7 * unit,
             s[0] + 27 * unit, s[1] + 8 * unit,
             s[0] + 9 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 7 * unit,
             s[0] + 8 * unit, s[1] + 7 * unit,
             s[0] + 8 * unit, s[1] + 8 * unit,
             s[0] + 7 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 27 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 8 * unit,
             s[0] - 27 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 9 * unit, s[1] + 10 * unit,
             s[0] + 27 * unit, s[1] + 10 * unit,
             s[0] + 27 * unit, s[1] + 11 * unit,
             s[0] + 9 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 2 * unit, s[1] + 10 * unit,
             s[0] + 8 * unit, s[1] + 10 * unit,
             s[0] + 8 * unit, s[1] + 11 * unit,
             s[0] - 2 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 10 * unit,
             s[0] - 3 * unit, s[1] + 10 * unit,
             s[0] - 3 * unit, s[1] + 11 * unit,
             s[0] - 4 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 5 * unit, s[1] + 10 * unit,
             s[0] - 27 * unit, s[1] + 10 * unit,
             s[0] - 27 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] + 9 * unit, s[1] + 13 * unit,
             s[0] + 27 * unit, s[1] + 13 * unit,
             s[0] + 27 * unit, s[1] + 14 * unit,
             s[0] + 9 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 13 * unit,
             s[0] + 8 * unit, s[1] + 13 * unit,
             s[0] + 8 * unit, s[1] + 14 * unit,
             s[0] + 7 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 27 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 14 * unit,
             s[0] - 27 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 9 * unit, s[1] + 16 * unit,
             s[0] + 27 * unit, s[1] + 16 * unit,
             s[0] + 27 * unit, s[1] + 17 * unit,
             s[0] + 9 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 2 * unit, s[1] + 16 * unit,
             s[0] + 8 * unit, s[1] + 16 * unit,
             s[0] + 8 * unit, s[1] + 17 * unit,
             s[0] - 2 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 16 * unit,
             s[0] - 3 * unit, s[1] + 16 * unit,
             s[0] - 3 * unit, s[1] + 17 * unit,
             s[0] - 4 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 5 * unit, s[1] + 16 * unit,
             s[0] - 27 * unit, s[1] + 16 * unit,
             s[0] - 27 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] + 9 * unit, s[1] + 19 * unit,
             s[0] + 27 * unit, s[1] + 19 * unit,
             s[0] + 27 * unit, s[1] + 20 * unit,
             s[0] + 9 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 19 * unit,
             s[0] + 8 * unit, s[1] + 19 * unit,
             s[0] + 8 * unit, s[1] + 20 * unit,
             s[0] + 7 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 27 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 20 * unit,
             s[0] - 27 * unit, s[1] + 20 * unit])

        early_termination_threshold = 233.24 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

    elif input_num == 36:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        #col 1
        obs.append(
            [s[0] - 4 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 2 * unit,
             s[0] - 4 * unit, s[1] + 2 * unit])

        obs.append(
            [s[0] - 16.5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 2 * unit,
             s[0] - 16.5 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 16.5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] - 4 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 8 * unit,
             s[0] - 4 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 7 * unit,
             s[0] - 5 * unit, s[1] + 7 * unit,
             s[0] - 5 * unit, s[1] + 8 * unit,
             s[0] - 16.5 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 16.5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] - 4 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 14 * unit,
             s[0] - 4 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 13 * unit,
             s[0] - 5 * unit, s[1] + 13 * unit,
             s[0] - 5 * unit, s[1] + 14 * unit,
             s[0] - 16.5 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 16.5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] - 4 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 20 * unit,
             s[0] - 4 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 19 * unit,
             s[0] - 5 * unit, s[1] + 19 * unit,
             s[0] - 5 * unit, s[1] + 20 * unit,
             s[0] - 16.5 * unit, s[1] + 20 * unit])

        early_termination_threshold = 285.74 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_alternate
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = 3.00

    elif input_num == 37:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        #col 1
        obs.append(
            [s[0] + 7 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 2 * unit,
             s[0] + 7 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 2 * unit,
             s[0] - 4 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 2 * unit,
             s[0] - 16.5 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 7 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 5 * unit,
             s[0] + 7 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 6 * unit, s[1] + 4 * unit,
             s[0] + 6 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 16.5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] + 7 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 8 * unit,
             s[0] + 7 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 8 * unit,
             s[0] - 4 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 7 * unit,
             s[0] - 5 * unit, s[1] + 7 * unit,
             s[0] - 5 * unit, s[1] + 8 * unit,
             s[0] - 16.5 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 7 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 11 * unit,
             s[0] + 7 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 6 * unit, s[1] + 10 * unit,
             s[0] + 6 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 16.5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] + 7 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 14 * unit,
             s[0] + 7 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 14 * unit,
             s[0] - 4 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 13 * unit,
             s[0] - 5 * unit, s[1] + 13 * unit,
             s[0] - 5 * unit, s[1] + 14 * unit,
             s[0] - 16.5 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 7 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 17 * unit,
             s[0] + 7 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 6 * unit, s[1] + 16 * unit,
             s[0] + 6 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 16.5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] + 7 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 20 * unit,
             s[0] + 7 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 20 * unit,
             s[0] - 4 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 19 * unit,
             s[0] - 5 * unit, s[1] + 19 * unit,
             s[0] - 5 * unit, s[1] + 20 * unit,
             s[0] - 16.5 * unit, s[1] + 20 * unit])

        early_termination_threshold = 224.0 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_alternate
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = 3.0

    elif input_num == 38:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        # baffles
        obs.append(
            [s[0] - 6 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 20 * unit,
             s[0] - 6 * unit, s[1] + 20 * unit])

        #col 1
        obs.append(
            [s[0] - 4 * unit, s[1] + 1 * unit,
             s[0] + 13.5 * unit, s[1] + 1 * unit,
             s[0] + 13.5 * unit, s[1] + 2 * unit,
             s[0] - 4 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 1 * unit,
             s[0] - 6 * unit, s[1] + 1 * unit,
             s[0] - 6 * unit, s[1] + 2 * unit,
             s[0] - 8 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 2 * unit,
             s[0] - 13.5 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 5 * unit,
             s[0] - 13.5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] - 4 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 8 * unit,
             s[0] - 4 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 8 * unit,
             s[0] - 13.5 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 11 * unit,
             s[0] - 13.5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] - 4 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 14 * unit,
             s[0] - 4 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 14 * unit,
             s[0] - 13.5 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 17 * unit,
             s[0] - 13.5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] - 4 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 20 * unit,
             s[0] - 4 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 20 * unit,
             s[0] - 13.5 * unit, s[1] + 20 * unit])

        early_termination_threshold = 265.52 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

    elif input_num == 39:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        # baffles
        obs.append(
            [s[0] - 6 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 20 * unit,
             s[0] - 6 * unit, s[1] + 20 * unit])

        #col 1
        obs.append(
            [s[0] + 7 * unit, s[1] + 1 * unit,
             s[0] + 13.5 * unit, s[1] + 1 * unit,
             s[0] + 13.5 * unit, s[1] + 2 * unit,
             s[0] + 7 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 2 * unit,
             s[0] - 4 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 1 * unit,
             s[0] - 6 * unit, s[1] + 1 * unit,
             s[0] - 6 * unit, s[1] + 2 * unit,
             s[0] - 8 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 2 * unit,
             s[0] - 13.5 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 7 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 5 * unit,
             s[0] + 7 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 6 * unit, s[1] + 4 * unit,
             s[0] + 6 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 5 * unit,
             s[0] - 13.5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] + 7 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 8 * unit,
             s[0] + 7 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 8 * unit,
             s[0] - 4 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 8 * unit,
             s[0] - 13.5 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 7 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 11 * unit,
             s[0] + 7 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 6 * unit, s[1] + 10 * unit,
             s[0] + 6 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 11 * unit,
             s[0] - 13.5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] + 7 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 14 * unit,
             s[0] + 7 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 14 * unit,
             s[0] - 4 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 14 * unit,
             s[0] - 13.5 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 7 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 17 * unit,
             s[0] + 7 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 6 * unit, s[1] + 16 * unit,
             s[0] + 6 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 17 * unit,
             s[0] - 13.5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] + 7 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 20 * unit,
             s[0] + 7 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 20 * unit,
             s[0] - 4 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 8 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 20 * unit,
             s[0] - 13.5 * unit, s[1] + 20 * unit])

        early_termination_threshold = 224.0 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

    elif input_num == 40:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        # baffles
        obs.append(
            [s[0] - 8 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 2 * unit,
             s[0] - 8 * unit, s[1] + 2 * unit])

        #col 1
        obs.append(
            [s[0] - 4 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 2 * unit,
             s[0] - 4 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 2 * unit,
             s[0] - 16.5 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 5 * unit,
             s[0] - 16.5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] - 4 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 8 * unit,
             s[0] - 4 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 8 * unit,
             s[0] - 16.5 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 11 * unit,
             s[0] - 16.5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] - 4 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 14 * unit,
             s[0] - 4 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 14 * unit,
             s[0] - 16.5 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 17 * unit,
             s[0] - 16.5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] - 4 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 20 * unit,
             s[0] - 4 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 20 * unit,
             s[0] - 16.5 * unit, s[1] + 20 * unit])

        early_termination_threshold = 265.52 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_alternate
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = 3.0

    elif input_num == 41:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        # baffles
        obs.append(
            [s[0] - 8 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 2 * unit,
             s[0] - 8 * unit, s[1] + 2 * unit])

        obs.append(
            [s[0] - 9.25 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 5 * unit,
             s[0] - 9 * unit, s[1] + 5 * unit,
             s[0] - 9 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 11 * unit,
             s[0] - 9 * unit, s[1] + 11 * unit,
             s[0] - 9 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 17 * unit,
             s[0] - 9 * unit, s[1] + 17 * unit,
             s[0] - 9 * unit, s[1] + 20 * unit,
             s[0] - 9.25 * unit, s[1] + 20 * unit])

        #col 1
        obs.append(
            [s[0] - 4 * unit, s[1] + 1 * unit,
             s[0] + 13.5 * unit, s[1] + 1 * unit,
             s[0] + 13.5 * unit, s[1] + 2 * unit,
             s[0] - 4 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 1 * unit,
             s[0] - 11 * unit, s[1] + 1 * unit,
             s[0] - 11 * unit, s[1] + 2 * unit,
             s[0] - 13.5 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 4 * unit,
             s[0] - 11 * unit, s[1] + 4 * unit,
             s[0] - 11 * unit, s[1] + 5 * unit,
             s[0] - 13.5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] - 4 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 8 * unit,
             s[0] - 4 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 7 * unit,
             s[0] - 11 * unit, s[1] + 7 * unit,
             s[0] - 11 * unit, s[1] + 8 * unit,
             s[0] - 13.5 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 10 * unit,
             s[0] - 11 * unit, s[1] + 10 * unit,
             s[0] - 11 * unit, s[1] + 11 * unit,
             s[0] - 13.5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] - 4 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 14 * unit,
             s[0] - 4 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 13 * unit,
             s[0] - 11 * unit, s[1] + 13 * unit,
             s[0] - 11 * unit, s[1] + 14 * unit,
             s[0] - 13.5 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 16 * unit,
             s[0] - 11 * unit, s[1] + 16 * unit,
             s[0] - 11 * unit, s[1] + 17 * unit,
             s[0] - 13.5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] - 4 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 20 * unit,
             s[0] - 4 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 19 * unit,
             s[0] - 11 * unit, s[1] + 19 * unit,
             s[0] - 11 * unit, s[1] + 20 * unit,
             s[0] - 13.5 * unit, s[1] + 20 * unit])

        early_termination_threshold = 265.52 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

    elif input_num == 42:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        # baffles
        obs.append(
            [s[0] - 8 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 2 * unit,
             s[0] - 8 * unit, s[1] + 2 * unit])

        #col 1
        obs.append(
            [s[0] + 7 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 2 * unit,
             s[0] + 7 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 2 * unit,
             s[0] - 4 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 2 * unit,
             s[0] - 16.5 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 7 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 5 * unit,
             s[0] + 7 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 6 * unit, s[1] + 4 * unit,
             s[0] + 6 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 5 * unit,
             s[0] - 16.5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] + 7 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 8 * unit,
             s[0] + 7 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 8 * unit,
             s[0] - 4 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 8 * unit,
             s[0] - 16.5 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 7 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 11 * unit,
             s[0] + 7 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 6 * unit, s[1] + 10 * unit,
             s[0] + 6 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 11 * unit,
             s[0] - 16.5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] + 7 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 14 * unit,
             s[0] + 7 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 14 * unit,
             s[0] - 4 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 14 * unit,
             s[0] - 16.5 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 7 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 17 * unit,
             s[0] + 7 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 6 * unit, s[1] + 16 * unit,
             s[0] + 6 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 17 * unit,
             s[0] - 16.5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] + 7 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 20 * unit,
             s[0] + 7 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 20 * unit,
             s[0] - 4 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 20 * unit,
             s[0] - 16.5 * unit, s[1] + 20 * unit])

        early_termination_threshold = 224.0 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_alternate
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = 3.0


    elif input_num == 43:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        # baffles
        obs.append(
            [s[0] + 5 * unit, s[1] + 1 * unit,
             s[0] + 8 * unit, s[1] + 1 * unit,
             s[0] + 8 * unit, s[1] + 2 * unit,
             s[0] + 6 * unit, s[1] + 2 * unit,
             s[0] + 6 * unit, s[1] + 7 * unit,
             s[0] + 8 * unit, s[1] + 7 * unit,
             s[0] + 8 * unit, s[1] + 8 * unit,
             s[0] + 6 * unit, s[1] + 8 * unit,
             s[0] + 6 * unit, s[1] + 13 * unit,
             s[0] + 8 * unit, s[1] + 13 * unit,
             s[0] + 8 * unit, s[1] + 14 * unit,
             s[0] + 6 * unit, s[1] + 14 * unit,
             s[0] + 6 * unit, s[1] + 19 * unit,
             s[0] + 8 * unit, s[1] + 19 * unit,
             s[0] + 8 * unit, s[1] + 20 * unit,
             s[0] + 5 * unit, s[1] + 20 * unit,
             s[0] + 5 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 5 * unit, s[1] + 16 * unit,
             s[0] + 5 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 5 * unit, s[1] + 10 * unit,
             s[0] + 5 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 5 * unit, s[1] + 4 * unit])

        #col 1
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 1 * unit,
             s[0] - 7 * unit, s[1] + 1 * unit,
             s[0] - 7 * unit, s[1] + 2 * unit,
             s[0] - 16.5 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 1 * unit,
             s[0] + 4 * unit, s[1] + 1 * unit,
             s[0] + 4 * unit, s[1] + 2 * unit,
             s[0] - 6 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] + 9 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 1 * unit,
             s[0] + 16.5 * unit, s[1] + 2 * unit,
             s[0] + 9 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 5 * unit,
             s[0] - 16.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 6 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 4 * unit,
             s[0] + 16.5 * unit, s[1] + 5 * unit,
             s[0] + 7 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 7 * unit,
             s[0] - 7 * unit, s[1] + 7 * unit,
             s[0] - 7 * unit, s[1] + 8 * unit,
             s[0] - 16.5 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 7 * unit,
             s[0] + 4 * unit, s[1] + 7 * unit,
             s[0] + 4 * unit, s[1] + 8 * unit,
             s[0] - 6 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] + 9 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 7 * unit,
             s[0] + 16.5 * unit, s[1] + 8 * unit,
             s[0] + 9 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 11 * unit,
             s[0] - 16.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 6 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 10 * unit,
             s[0] + 16.5 * unit, s[1] + 11 * unit,
             s[0] + 7 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 13 * unit,
             s[0] - 7 * unit, s[1] + 13 * unit,
             s[0] - 7 * unit, s[1] + 14 * unit,
             s[0] - 16.5 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 13 * unit,
             s[0] + 4 * unit, s[1] + 13 * unit,
             s[0] + 4 * unit, s[1] + 14 * unit,
             s[0] - 6 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] + 9 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 13 * unit,
             s[0] + 16.5 * unit, s[1] + 14 * unit,
             s[0] + 9 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 17 * unit,
             s[0] - 16.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 6 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 16 * unit,
             s[0] + 16.5 * unit, s[1] + 17 * unit,
             s[0] + 7 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] - 16.5 * unit, s[1] + 19 * unit,
             s[0] - 7 * unit, s[1] + 19 * unit,
             s[0] - 7 * unit, s[1] + 20 * unit,
             s[0] - 16.5 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 19 * unit,
             s[0] + 4 * unit, s[1] + 19 * unit,
             s[0] + 4 * unit, s[1] + 20 * unit,
             s[0] - 6 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] + 9 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 19 * unit,
             s[0] + 16.5 * unit, s[1] + 20 * unit,
             s[0] + 9 * unit, s[1] + 20 * unit])

        early_termination_threshold = 224.0 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_alternate
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = 3.0

    elif input_num == 44:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        # baffles
        obs.append(
            [s[0] - 8 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 1 * unit,
             s[0] - 5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 5 * unit,
             s[0] - 5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 11 * unit,
             s[0] - 5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 17 * unit,
             s[0] - 5 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 20 * unit,
             s[0] - 8 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 19 * unit,
             s[0] - 6 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 14 * unit,
             s[0] - 8 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 13 * unit,
             s[0] - 6 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 8 * unit,
             s[0] - 8 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 7 * unit,
             s[0] - 6 * unit, s[1] + 2 * unit,
             s[0] - 8 * unit, s[1] + 2 * unit])

        #col 1
        obs.append(
            [s[0] + 7 * unit, s[1] + 1 * unit,
             s[0] + 12.5 * unit, s[1] + 1 * unit,
             s[0] + 12.5 * unit, s[1] -2 * unit,
             s[0] + 13.5 * unit, s[1] - 2 * unit,
             s[0] + 13.5 * unit, s[1] + 2 * unit,
             s[0] + 7 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 1 * unit,
             s[0] + 6 * unit, s[1] + 2 * unit,
             s[0] - 4 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] - 2 * unit,
             s[0] - 12.5 * unit, s[1] - 2 * unit,
             s[0] - 12.5 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 1 * unit,
             s[0] - 9 * unit, s[1] + 2 * unit,
             s[0] - 13.5 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 7 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 5 * unit,
             s[0] + 7 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 6 * unit, s[1] + 4 * unit,
             s[0] + 6 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 5 * unit,
             s[0] - 13.5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] + 7 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 8 * unit,
             s[0] + 7 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 7 * unit,
             s[0] + 6 * unit, s[1] + 8 * unit,
             s[0] - 4 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 7 * unit,
             s[0] - 9 * unit, s[1] + 8 * unit,
             s[0] - 13.5 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 7 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 11 * unit,
             s[0] + 7 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 6 * unit, s[1] + 10 * unit,
             s[0] + 6 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 11 * unit,
             s[0] - 13.5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] + 7 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 14 * unit,
             s[0] + 7 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 13 * unit,
             s[0] + 6 * unit, s[1] + 14 * unit,
             s[0] - 4 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 13 * unit,
             s[0] - 9 * unit, s[1] + 14 * unit,
             s[0] - 13.5 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 7 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 17 * unit,
             s[0] + 7 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 6 * unit, s[1] + 16 * unit,
             s[0] + 6 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 17 * unit,
             s[0] - 13.5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] + 7 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 23 * unit,
             s[0] + 12.5 * unit, s[1] + 23 * unit,
             s[0] + 12.5 * unit, s[1] + 20 * unit,
             s[0] + 7 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 4 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 19 * unit,
             s[0] + 6 * unit, s[1] + 20 * unit,
             s[0] - 4 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 19 * unit,
             s[0] - 9 * unit, s[1] + 20 * unit,
             s[0] - 12.5 * unit, s[1] + 20 * unit,
             s[0] - 12.5 * unit, s[1] + 23 * unit,
             s[0] - 13.5 * unit, s[1] + 23 * unit])

        early_termination_threshold = 224.0 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

    elif input_num == 45:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        # baffles
        obs.append(
            [s[0] + 5 * unit, s[1] + 1 * unit,
             s[0] + 8 * unit, s[1] + 1 * unit,
             s[0] + 8 * unit, s[1] + 2 * unit,
             s[0] + 6 * unit, s[1] + 2 * unit,
             s[0] + 6 * unit, s[1] + 7 * unit,
             s[0] + 8 * unit, s[1] + 7 * unit,
             s[0] + 8 * unit, s[1] + 8 * unit,
             s[0] + 6 * unit, s[1] + 8 * unit,
             s[0] + 6 * unit, s[1] + 13 * unit,
             s[0] + 8 * unit, s[1] + 13 * unit,
             s[0] + 8 * unit, s[1] + 14 * unit,
             s[0] + 6 * unit, s[1] + 14 * unit,
             s[0] + 6 * unit, s[1] + 19 * unit,
             s[0] + 8 * unit, s[1] + 19 * unit,
             s[0] + 8 * unit, s[1] + 20 * unit,
             s[0] + 5 * unit, s[1] + 20 * unit,
             s[0] + 5 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit,
             s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 5 * unit, s[1] + 16 * unit,
             s[0] + 5 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit,
             s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 5 * unit, s[1] + 10 * unit,
             s[0] + 5 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit,
             s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 5 * unit, s[1] + 4 * unit])

        #col 1
        obs.append(
            [s[0] - 13.5 * unit, s[1] - 2 * unit,
             s[0] - 12.5 * unit, s[1] - 2 * unit,
             s[0] - 12.5 * unit, s[1] + 1 * unit,
             s[0] - 7 * unit, s[1] + 1 * unit,
             s[0] - 7 * unit, s[1] + 2 * unit,
             s[0] - 13.5 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 1 * unit,
             s[0] + 4 * unit, s[1] + 1 * unit,
             s[0] + 4 * unit, s[1] + 2 * unit,
             s[0] - 6 * unit, s[1] + 2 * unit])
        obs.append(
            [s[0] + 9 * unit, s[1] + 1 * unit,
             s[0] + 12.5 * unit, s[1] + 1 * unit,
             s[0] + 12.5 * unit, s[1] - 2 * unit,
             s[0] + 13.5 * unit, s[1] - 2 * unit,
             s[0] + 13.5 * unit, s[1] + 2 * unit,
             s[0] + 9 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 4 * unit,
             s[0] - 7 * unit, s[1] + 5 * unit,
             s[0] - 13.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 4 * unit,
             s[0] - 0.5 * unit, s[1] + 5 * unit,
             s[0] - 6 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 5 * unit,
             s[0] + 7 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] -13.5 * unit, s[1] + 7 * unit,
             s[0] - 7 * unit, s[1] + 7 * unit,
             s[0] - 7 * unit, s[1] + 8 * unit,
             s[0] - 13.5 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 7 * unit,
             s[0] + 4 * unit, s[1] + 7 * unit,
             s[0] + 4 * unit, s[1] + 8 * unit,
             s[0] - 6 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] + 9 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 8 * unit,
             s[0] + 9 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 10 * unit,
             s[0] - 7 * unit, s[1] + 11 * unit,
             s[0] - 13.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 10 * unit,
             s[0] - 0.5 * unit, s[1] + 11 * unit,
             s[0] - 6 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 11 * unit,
             s[0] + 7 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 13 * unit,
             s[0] - 7 * unit, s[1] + 13 * unit,
             s[0] - 7 * unit, s[1] + 14 * unit,
             s[0] - 13.5 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 13 * unit,
             s[0] + 4 * unit, s[1] + 13 * unit,
             s[0] + 4 * unit, s[1] + 14 * unit,
             s[0] - 6 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] + 9 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 14 * unit,
             s[0] + 9 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 16 * unit,
             s[0] - 7 * unit, s[1] + 17 * unit,
             s[0] - 13.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 16 * unit,
             s[0] - 0.5 * unit, s[1] + 17 * unit,
             s[0] - 6 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] + 7 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 17 * unit,
             s[0] + 7 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 19 * unit,
             s[0] - 7 * unit, s[1] + 19 * unit,
             s[0] - 7 * unit, s[1] + 20 * unit,
             s[0] - 12.5 * unit, s[1] + 20 * unit,
             s[0] - 12.5 * unit, s[1] + 23 * unit,
             s[0] - 13.5 * unit, s[1] + 23 * unit])
        obs.append(
            [s[0] - 6 * unit, s[1] + 19 * unit,
             s[0] + 4 * unit, s[1] + 19 * unit,
             s[0] + 4 * unit, s[1] + 20 * unit,
             s[0] - 6 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] + 9 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 23 * unit,
             s[0] + 12.5 * unit, s[1] + 23 * unit,
             s[0] + 12.5 * unit, s[1] + 20 * unit,
             s[0] + 9 * unit, s[1] + 20 * unit])

        early_termination_threshold = 224.0 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

    elif input_num == 46:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        #col 1
        obs.append(
            [s[0] - 0.5 * unit, s[1] + 1 * unit,
             s[0] + 13.5 * unit, s[1] + 1 * unit,
             s[0] + 13.5 * unit, s[1] + 2 * unit,
             s[0] - 0.5 * unit, s[1] + 2 * unit])

        obs.append(
            [s[0] - 13.5 * unit, s[1] + 1 * unit,
             s[0] - 1.5 * unit, s[1] + 1 * unit,
             s[0] - 1.5 * unit, s[1] + 2 * unit,
             s[0] - 13.5 * unit, s[1] + 2 * unit])
        # col 2
        obs.append(
            [s[0] + 1.5 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 4 * unit,
             s[0] + 13.5 * unit, s[1] + 5 * unit,
             s[0] + 1.5 * unit, s[1] + 5 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 4 * unit,
             s[0] + 0.5 * unit, s[1] + 4 * unit,
             s[0] + 0.5 * unit, s[1] + 5 * unit,
             s[0] - 13.5 * unit, s[1] + 5 * unit])
        # col 3
        obs.append(
            [s[0] - 0.5 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 7 * unit,
             s[0] + 13.5 * unit, s[1] + 8 * unit,
             s[0] - 0.5 * unit, s[1] + 8 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 7 * unit,
             s[0] - 1.5 * unit, s[1] + 7 * unit,
             s[0] - 1.5 * unit, s[1] + 8 * unit,
             s[0] - 13.5 * unit, s[1] + 8 * unit])
        # col 4
        obs.append(
            [s[0] + 1.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 11 * unit,
             s[0] + 1.5 * unit, s[1] + 11 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 10 * unit,
             s[0] + 0.5 * unit, s[1] + 10 * unit,
             s[0] + 0.5 * unit, s[1] + 11 * unit,
             s[0] - 13.5 * unit, s[1] + 11 * unit])
        # col 5
        obs.append(
            [s[0] - 0.5 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 13 * unit,
             s[0] + 13.5 * unit, s[1] + 14 * unit,
             s[0] - 0.5 * unit, s[1] + 14 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 13 * unit,
             s[0] - 1.5 * unit, s[1] + 13 * unit,
             s[0] - 1.5 * unit, s[1] + 14 * unit,
             s[0] - 13.5 * unit, s[1] + 14 * unit])
        # col 6
        obs.append(
            [s[0] + 1.5 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 16 * unit,
             s[0] + 13.5 * unit, s[1] + 17 * unit,
             s[0] + 1.5 * unit, s[1] + 17 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 16 * unit,
             s[0] + 0.5 * unit, s[1] + 16 * unit,
             s[0] + 0.5 * unit, s[1] + 17 * unit,
             s[0] - 13.5 * unit, s[1] + 17 * unit])
        # col 7
        obs.append(
            [s[0] - 0.5 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 19 * unit,
             s[0] + 13.5 * unit, s[1] + 20 * unit,
             s[0] - 0.5 * unit, s[1] + 20 * unit])
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 19 * unit,
             s[0] - 1.5 * unit, s[1] + 19 * unit,
             s[0] - 1.5 * unit, s[1] + 20 * unit,
             s[0] - 13.5 * unit, s[1] + 20 * unit])

        early_termination_threshold = 165 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor

    elif input_num == 47:
        # custom start and end points with round and evenly divisible values
        s = start = [28.031110, -81.946130]
        end = [28.031110, -81.944660]

        unit = 0.000070

        # obstacles are grouped in 7 rectangular columns, and are listed as they appear in the image, top to bottom
        # obstacles are defined clockwise starting from the bottom left vertex ( all obstacles are rectangular)
        # col 4

        obs.append(
            [s[0] - 3 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 10 * unit,
             s[0] + 13.5 * unit, s[1] + 11 * unit,
             s[0] - 3 * unit, s[1] + 11 * unit])
        '''
        obs.append(
            [s[0] - 13.5 * unit, s[1] + 10 * unit,
             s[0] + 2 * unit, s[1] + 10 * unit,
             s[0] + 2 * unit, s[1] + 11 * unit,
             s[0] - 13.5 * unit, s[1] + 11 * unit])
        '''

        early_termination_threshold = 155 * 1.05  

        # *** *** *** ***
        Params.start_point = start
        Params.end_point = end
        Params.obstacles = obstacles_list_to_objects(obs)
        Params.goals = goals
        Params.origin = origin_green
        Params.early_termination_threshold = early_termination_threshold
        Params.scale_factor = scale_factor


    else:
        sys.stderr.write("Input argument {} is out of bounds!".format(input_num))
        raise SystemExit(1)


def obstacles_list_to_objects(obs):
    obstacle_class_list = []
    for i, obstacle in enumerate(obs):
        if len(obstacle) == 3:
            obstacle_class_list.append(Obstacle("C", obstacle))
        elif len(obstacle) == 4:
            obstacle_class_list.append(Obstacle("L", obstacle))
        elif len(obstacle) >= 6:
            obstacle_class_list.append(Obstacle("P", obstacle))
        else:
            print "INPUT DEBUG ERROR"
    return obstacle_class_list
