from random import randint

from params import Params
import utilities


class ObstacleSegment:
    def __init__(self, start, end, extended_start, extended_end, length):
        self.cw = end
        self.ccw = start
        self.ecw = extended_end
        self.eccw = extended_start
        self.len = length


class Intersection:
    def __init__(self, point, type, obstacle, path, ob_seg_index, path_seg_index):
        self.point = point
        self.ob_seg = ob_seg_index
        self.path_seg = path_seg_index
        self.extended_point = self.calculate_extended_point(obstacle, path)
        self.distance = self.calculate_distance(path)
        self.type = type

    def calculate_extended_point(self, obs, path):
        # step 1 find the slope of the entering/exiting path segment
        dy = path[self.path_seg + 1][0] - path[self.path_seg][0]
        dx = path[self.path_seg + 1][1] - path[self.path_seg][1]

        # step 2 find intersections of circle and line
        xy_plus, xy_minus = utilities.line_through_circle_intersections(utilities.convert(self.point), dy, dx,
                                                                        obs.buffer_dist * utilities.degrees_per_meter)
        xy_plus = utilities.convert(xy_plus)  # now yx
        xy_minus = utilities.convert(xy_minus)  # now yx

        # step 5 determine which of the two points (plus or minus) is inside the obstacle. return the OUTSIDE point
        #   NOTE returned point is converted back to lat/lon ordering i.e. Y/X format must check xy_p in xy form
        if obs.contains_point(utilities.convert(xy_plus)):  # TODO REPLACE THIS METHOD WITH DISTANCE FROM START/END
            return xy_minus
        else:
            return xy_plus

    def calculate_distance(self, path):
        return utilities.distance_formula(self.point, path[self.path_seg])


class Obstacle:
    def __init__(self, ob_type, data, buffer_dist_meters=0.5):
        self.type = ob_type
        self.legacy_data = data
        if self.type == "C":
            assert len(data) == 3
            self.center = [data[0], data[1]]
            self.radius = data[2]
        elif self.type == "L":
            assert len(data) == 4
            self.start_point = [data[0], data[1]]
            self.end_point = [data[2], data[3]]
        elif self.type == "P":
            assert len(data) >= 6 and len(data) % 2 == 0
            self.vertices = self.pair_values_together(data)
            self.buffer_dist = buffer_dist_meters
            self.vertex_count = len(self.vertices)
            self.perimeter = self.calculate_perimeter()
            self.max_x, self.max_y, self.min_x, self.min_y = self.calculate_max_min_x_y()
            self.segments = self.build_segment_list()
        else:
            print "ERROR: INVALID OBSTACLE TYPE"

    def pair_values_together(self, list):
        assert len(list) % 2 == 0 and len(list) > 0
        list_of_pairs = []
        hold = None
        toggle = 0
        for val in list:
            if toggle == 0:
                hold = val
                toggle = 1
            else:
                list_of_pairs.append([hold, val])
                toggle = 0
        return list_of_pairs

    def calculate_perimeter(self):
        length_sum = 0
        for i in xrange(self.vertex_count):
            length_sum += utilities.distance_formula(self.vertices[i], self.vertices[(i + 1) % self.vertex_count])
        return length_sum

    def calculate_max_min_x_y(self):
        max_x = -1.e10
        max_y = -1.e10
        min_x = 1.e10
        min_y = 1.e10

        for vertex in self.vertices:
            if vertex[1] > max_x:
                max_x = vertex[1]
            if vertex[1] < min_x:
                min_x = vertex[1]

            if vertex[0] > max_y:
                max_y = vertex[0]
            if vertex[0] < min_y:
                min_y = vertex[0]

        return max_x, max_y, min_x, min_y

    def contains_point(self, p_xy):
        # print "self.contains_point({})".format(p_xy)
        # does the point exist outside of the bounding box of the obstacle? if so, return false
        if p_xy[0] > self.max_x or p_xy[0] < self.min_x or p_xy[1] > self.max_y or p_xy[1] < self.min_y:
            return False

        # define an epsilon which will give padding to the ray used for the ray casting method of point inside detection
        e = (self.max_x - self.min_x) / 10.

        ray_lr_start = [self.min_x - e, p_xy[1]]
        ray_rl_end = [self.max_x + e, p_xy[1]]

        # count up the number of intersections  the ray has with the polygon sides
        intersections_lr = 0
        intersections_rl = 0
        for i in xrange(self.vertex_count):
            # test left to right ending at point
            intrsct_type, intrsct_point = utilities.find_intersection_point(ray_lr_start, p_xy,
                                                                            utilities.convert(self.vertices[i]),
                                                                            utilities.convert(self.vertices[(
                                                                                                                i + 1) % self.vertex_count]))
            # print intrsct_type
            intersections_lr += 1 if intrsct_type not in [0, 1, 3, 4] else 0
            # test right to left ending at ray_end
            intrsct_type, intrsct_point = utilities.find_intersection_point(p_xy, ray_rl_end,
                                                                            utilities.convert(self.vertices[i]),
                                                                            utilities.convert(self.vertices[(
                                                                                                                i + 1) % self.vertex_count]))
            # print intrsct_type
            intersections_rl += 1 if intrsct_type not in [0, 1, 3, 4] else 0
        # print "contains_point lr ints: {}".format(intersections_lr)
        # print "contains_point rl ints: {}".format(intersections_rl)
        return intersections_lr % 2 == 1 or intersections_rl % 2 == 1

    def calculate_extended_point(self, p1, p2, p3):
        # step 1 translate both vectors at the origin
        # NOTE in this step we also convert from lat/lon to x/y format to keep the math conventional
        #   the final return value will be converted back to lat/lon so it can be used as a path waypoint directly
        x1 = p2[1] - p1[1]
        y1 = p2[0] - p1[0]
        # NOTE using p2 as the endpoint for both segments ensures we calculate the correct angle bisector
        x2 = p2[1] - p3[1]
        y2 = p2[0] - p3[0]

        # step 2 calculate the x and y values for the angle bisecting vector
        # length of vector 1
        len_1 = utilities.distance_formula([0, 0], [x1, y1])
        # length of vector 2
        len_2 = utilities.distance_formula([0, 0], [x2, y2])
        # x value of angle bisector
        bx = (x1 / len_1) + (x2 / len_2)
        # y value of angle bisector
        by = (y1 / len_1) + (y2 / len_2)
        # print "calc_exted_pt p2: {}".format(p2)
        # step 3 find intersection points of angle bisector and a circle of radius self.buffer_dist
        xy_plus, xy_minus = utilities.line_through_circle_intersections(utilities.convert(p2), by, bx,
                                                                        self.buffer_dist * utilities.degrees_per_meter)
        # print "xy_plus {}".format(xy_plus)
        p_plus = utilities.convert(xy_plus)  # now in yx form
        p_minus = utilities.convert(xy_minus)  # now in yx form

        # step 4 determine which of the two points (plus or minus) is inside the obstacle. return the OUTSIDE point
        #   NOTE returned point is converted back to lat/lon ordering i.e. Y/X format but needs to be checked in xy
        if self.contains_point(utilities.convert(p_plus)):
            return p_minus
        else:
            return p_plus

    def build_segment_list(self):
        segments = []
        for i in xrange(self.vertex_count):
            start = i
            end = (i + 1) % self.vertex_count
            prev_start = (i - 1) % self.vertex_count
            next_end = (i + 2) % self.vertex_count

            length = utilities.distance_formula(self.vertices[start], self.vertices[end])

            extended_start = self.calculate_extended_point(self.vertices[prev_start], self.vertices[start],
                                                           self.vertices[end])

            extended_end = self.calculate_extended_point(self.vertices[start], self.vertices[end],
                                                         self.vertices[next_end])

            segments.append(
                ObstacleSegment(start, end, extended_start, extended_end, length))
        return segments

    def epsilon_vertex_test(self, intersection_p, dy, dx, e=.1):
        intersection_p = utilities.convert(intersection_p)  # NOTE converted to x y form
        # print "FEVT point {}".format(intersection_p)
        p1, p2 = utilities.line_through_circle_intersections(intersection_p, dy, dx, e * utilities.degrees_per_meter)

        p1_inside = self.contains_point(utilities.convert(p1))
        p2_inside = self.contains_point(utilities.convert(p2))
        # print "in in" if not p1_inside and p2_inside else ""
        return (p1_inside and not p2_inside) or (not p1_inside and p2_inside)

    def half_epsilon_vertex_test(self, intersection_p, dy, dx, direction_p, e=.1):
        # print "H.E.T."
        intersection_p = utilities.convert(intersection_p)  # now in x y
        # print intersection_p
        p1, p2 = utilities.line_through_circle_intersections(intersection_p, dy, dx, e * utilities.degrees_per_meter)
        p1 = utilities.convert(p1)  # yx
        p2 = utilities.convert(p2)  # yx
        # print p1, p2
        p1_dist = utilities.distance_formula(p1, direction_p)
        p2_dist = utilities.distance_formula(p2, direction_p)
        # print p1_dist, p2_dist

        # print "radius {} | diameter/2 {}".format(e*utilities.degrees_per_meter, math.fabs(p1_dist - p2_dist))

        if p1_dist < p2_dist:
            # print "returning p1.inside"
            return self.contains_point(utilities.convert(p1))
        else:
            # print "returning p2.inside"
            return self.contains_point(utilities.convert(p2))

    def make_valid_path(self, path):
        if self.type == "P":

            def list_intersections(p_seg):
                slist = []
                for ob_seg in self.segments:
                    t, p = utilities.find_intersection_point(path[p_seg], path[p_seg + 1],
                                                             self.vertices[ob_seg.ccw],
                                                             self.vertices[ob_seg.cw])
                    if t not in [0, 3]:
                        if t in [1, 4]:
                            # print "Type {}".format(t)
                            dx = self.vertices[ob_seg.cw][1] - self.vertices[ob_seg.ccw][1]
                            dy = self.vertices[ob_seg.cw][0] - self.vertices[ob_seg.ccw][0]

                            # NOTE the slope parameters are reversed and one is negative to give the normal slope
                            p1, p2 = utilities.line_through_circle_intersections(utilities.convert(path[p_seg + 1]),
                                                                                 -dx, dy,
                                                                                 1 * utilities.degrees_per_meter)
                            # note: p1 and p2 are in x,y form when returned from line_through_circle
                            if self.contains_point(p1):  # contains check needs to be in x y form
                                path[p_seg + 1] = utilities.convert(p2)
                            else:
                                path[p_seg + 1] = utilities.convert(p1)
                            return list_intersections(p_seg)
                        else:
                            slist.append(Intersection(p, t, self, path, ob_seg.ccw, p_seg))
                return slist

            # END DEF
            work = []
            for path_seg in xrange(len(path) - 1):
                sorting_list = list_intersections(path_seg)
                sorting_list.sort(reverse=False, key=lambda intrsctns: intrsctns.distance)
                work.extend(sorting_list)
            # END PATHSEG LOOP

            # check if 2 points are equal, if so do epsilon vertex test, if test returns true delete one of the points
            i = 0
            while i <= len(work) - 2:
                if utilities.pts_equal(work[i].point, work[i + 1].point):
                    # print "POINTS EQUAL"
                    if work[i].type != 7 and work[i + 1].type != 7:
                        # print "NO SEVENS"
                        s1 = work[i].path_seg
                        # print "s1 {}".format(s1)
                        dy1 = (path[s1 + 1][0] - path[s1][0])
                        dx1 = (path[s1 + 1][1] - path[s1][1])
                        # print "dy1/dx1 = {}/{}".format(dy1, dx1)
                        if work[i].type == 6:
                            # print "type 6 - full epsilon test"
                            epsilon_test_fail = self.epsilon_vertex_test(work[i].point, dy1, dx1)
                        else:
                            # print "Type 2 or 5 - half epsilon test"
                            s2 = work[i + 1].path_seg
                            # print "s2 {}".format(s2)
                            # print "work[i].point == path[s2] {}".format(work[i].point == path[s2])
                            # print "work[i+1].point == path[s2] {}".format(work[i + 1].point == path[s2])
                            # print "work[i].path_seg {}".format(work[i].path_seg)
                            # print "work[i + 1].path_seg {}".format(work[i + 1].path_seg)
                            dy2 = (path[s2 + 1][0] - path[s2][0])
                            dx2 = (path[s2 + 1][1] - path[s2][1])

                            # print "dy2/dx2 = {}/{}".format(dy2, dx2)
                            test1 = self.half_epsilon_vertex_test(work[i + 1].point, dy1, dx1, path[s1])
                            # print "Test 1 {}".format(test1)
                            test2 = self.half_epsilon_vertex_test(work[i + 1].point, dy2, dx2, path[s2 + 1])
                            # print "Test 2 {}".format(test2)
                            epsilon_test_fail = (test1 and not test2) or (not test1 and test2)
                        if epsilon_test_fail:
                            # print "DELETE {}".format(work[i + 1].point)
                            del work[i + 1]
                            # print "NEW Work: {}".format(len(work))
                i += 1

            if len(work) % 2 == 1:  # TODO this code block is just for printing data ( now returns as a failsafe)
                print "Work {}".format(len(work))
                for elt in work:
                    print elt.point
                    print "Type {}".format(elt.type)
                    print "Path Seg {}".format(elt.path_seg)
                    print "Obs  Seg {}".format(elt.ob_seg)
                return path

            # process the work queue to construct the new path around self
            # index offset variable keeps track of where to insert into the path
            net = 0
            insert = 1
            deleted = 0
            while len(work) > 0:
                net = net + insert - deleted - 1
                insert = 1
                deleted = 0
                # grab an in/out pair
                in_intersection = work[0]
                in_pi = in_intersection.path_seg
                in_oi = in_intersection.ob_seg
                out_intersection = work[1]
                out_pi = out_intersection.path_seg
                out_oi = out_intersection.ob_seg
                # remove the pair from the work queue
                del work[0:2]  # note this deletes 0 to 1 because the form is del[a:b+1] to delete a to b
                # if there are waypoints inside the obstacle between intersection points, delete them
                if in_pi != out_pi:
                    del path[
                        in_pi + 1 + net: out_pi + 1 + net]  # NOTE this removes all points between in and out including out
                    deleted += out_pi - in_pi

                # insert the first intersection point (extended) here.
                path.insert(in_pi + insert + net, in_intersection.extended_point)
                insert += 1  # after each insert you must increment this count to preserve the waypoint ordering
                # next determine if the path will travel past any vertex of the obstacle by checking ob side indices
                # if the two intersections were on the same side of the obstacle this if will be skipped and the
                # exiting intersection point will be inserted. otherwise we need to insert extra points between
                # the just inserted entrance intersection and the exiting intersection to go around the vertices
                if in_oi != out_oi:
                    # do a clockwise walk and calculate the length of the perimeter segment between in and out
                    cw_perim = 0
                    # manually calculate len from intersection to first vertex clockwise
                    cw_perim += utilities.distance_formula(in_intersection.point,
                                                           self.vertices[self.segments[in_oi].cw])
                    # incriment the seg_index variable mod vertex_count to allow circular travel through the list
                    # start at in_oi and add 1 to move CW from the segment we started on.
                    # NOTE adding 1 is equivelant to walking clockwise and subtracting 1 is equivelant to CCW travel
                    seg_index = (in_oi + 1) % self.vertex_count
                    # add the length of each segment which is entirely included in the cw walk
                    while seg_index != out_oi:
                        cw_perim += self.segments[seg_index].len
                        seg_index = (seg_index + 1) % self.vertex_count
                    # manually add the length of the segment between the exit intersection and the point CCW from it
                    cw_perim += utilities.distance_formula(self.vertices[self.segments[out_oi].ccw],
                                                           out_intersection.point)
                    # if the CW walk length is larger than half the total perimeter of self, add points from the CCW
                    # walk, otherwise the CW walk is shorter and we add points from the CW walk
                    if cw_perim > 0.5 * self.perimeter:
                        # add extended vertex points traveling CCW from in to out
                        add_index = in_oi
                        while add_index != out_oi:
                            path.insert(in_pi + insert + net, self.segments[add_index].eccw)
                            insert += 1
                            add_index = (add_index - 1) % self.vertex_count
                    else:
                        # add points traveling cw from in to out
                        add_index = in_oi
                        while add_index != out_oi:
                            path.insert(in_pi + insert + net, self.segments[add_index].ecw)
                            insert += 1
                            add_index = (add_index + 1) % self.vertex_count
                # insert the exiting intersection's extended point here
                path.insert(in_pi + insert + net, out_intersection.extended_point)
                insert += 1
            return path
        else:
            # print "Non-Polygon"
            return path

    # @profile
    def path_intrusion(self, path):
        if self.type == "P":
            intrusion = 0

            def list_intersections(p_seg):
                slist = []
                for ob_seg in self.segments:
                    t, p = utilities.find_intersection_point(path[p_seg], path[p_seg + 1],
                                                             self.vertices[ob_seg.ccw],
                                                             self.vertices[ob_seg.cw])
                    if t not in [0, 3]:
                        if t in [1, 4]:
                            # print "Type {}".format(t)
                            dx = self.vertices[ob_seg.cw][1] - self.vertices[ob_seg.ccw][1]
                            dy = self.vertices[ob_seg.cw][0] - self.vertices[ob_seg.ccw][0]

                            # NOTE the slope parameters are reversed and one is negative to give the normal slope
                            p1, p2 = utilities.line_through_circle_intersections(utilities.convert(path[p_seg + 1]),
                                                                                 -dx, dy,
                                                                                 1 * utilities.degrees_per_meter)
                            # note: p1 and p2 are in x,y form when returned from line_through_circle
                            if self.contains_point(p1):  # contains check needs to be in x y form
                                path[p_seg + 1] = utilities.convert(p2)
                            else:
                                path[p_seg + 1] = utilities.convert(p1)
                            return list_intersections(p_seg)
                        else:
                            slist.append(Intersection(p, t, self, path, ob_seg.ccw, p_seg))
                return slist

            # END DEF
            work = []
            for path_seg in xrange(len(path) - 1):
                sorting_list = list_intersections(path_seg)
                sorting_list.sort(reverse=False, key=lambda intrsctns: intrsctns.distance)
                work.extend(sorting_list)
            # END PATHSEG LOOP

            # check if 2 points are equal, if so do epsilon vertex test, if test returns true delete one of the points
            i = 0
            while i <= len(work) - 2:
                if utilities.pts_equal(work[i].point, work[i + 1].point):
                    # print "POINTS EQUAL"
                    if work[i].type != 7 and work[i + 1].type != 7:
                        # print "NO SEVENS"
                        s1 = work[i].path_seg
                        # print "s1 {}".format(s1)
                        dy1 = (path[s1 + 1][0] - path[s1][0])
                        dx1 = (path[s1 + 1][1] - path[s1][1])
                        # print "dy1/dx1 = {}/{}".format(dy1, dx1)
                        if work[i].type == 6:
                            # print "type 6 - full epsilon test"
                            epsilon_test_fail = self.epsilon_vertex_test(work[i].point, dy1, dx1)
                        else:
                            # print "Type 2 or 5 - half epsilon test"
                            s2 = work[i + 1].path_seg
                            # print "s2 {}".format(s2)
                            # print "work[i].point == path[s2] {}".format(work[i].point == path[s2])
                            # print "work[i+1].point == path[s2] {}".format(work[i + 1].point == path[s2])
                            # print "work[i].path_seg {}".format(work[i].path_seg)
                            # print "work[i + 1].path_seg {}".format(work[i + 1].path_seg)
                            dy2 = (path[s2 + 1][0] - path[s2][0])
                            dx2 = (path[s2 + 1][1] - path[s2][1])

                            # print "dy2/dx2 = {}/{}".format(dy2, dx2)
                            test1 = self.half_epsilon_vertex_test(work[i + 1].point, dy1, dx1, path[s1])
                            # print "Test 1 {}".format(test1)
                            test2 = self.half_epsilon_vertex_test(work[i + 1].point, dy2, dx2, path[s2 + 1])
                            # print "Test 2 {}".format(test2)
                            epsilon_test_fail = (test1 and not test2) or (not test1 and test2)
                        if epsilon_test_fail:
                            # print "DELETE {}".format(work[i + 1].point)
                            del work[i + 1]
                            # print "NEW Work: {}".format(len(work))
                i += 1

            if len(work) % 2 == 1:  # TODO this code block is just for printing data ( now returns as a failsafe)
                print
                print "Work {}".format(len(work))
                for elt in work:
                    print elt.point
                    print "Type {}".format(elt.type)
                    print "Path Seg {}".format(elt.path_seg)
                    print "Obs  Seg {}".format(elt.ob_seg)
                # TODO - not 100% convinced that returning 1e8 is the right thing to do
                return 1e8

            # process the work queue to construct the new path around self
            # index offset variable keeps track of where to insert into the path
            net = 0
            insert = 1
            deleted = 0
            while len(work) > 0:
                inserted_points = []
                deleted_points = []
                net = net + insert - deleted - 1
                insert = 1
                deleted = 0
                # grab an in/out pair
                in_intersection = work[0]
                in_pi = in_intersection.path_seg
                in_oi = in_intersection.ob_seg
                out_intersection = work[1]
                out_pi = out_intersection.path_seg
                out_oi = out_intersection.ob_seg
                # remove the pair from the work queue
                del work[0:2]  # note this deletes 0 to 1 because the form is del[a:b+1] to delete a to b
                # if there are waypoints inside the obstacle between intersection points, delete them
                if in_pi != out_pi:
                    deleted_points.extend(path[in_pi + 1 + net: out_pi + 1 + net])

                # insert the first intersection point (extended) here.
                inserted_points.append(in_intersection.point)

                # TODO commented following line while debugging
                # insert += 1  # after each insert you must increment this count to preserve the waypoint ordering

                # next determine if the path will travel past any vertex of the obstacle by checking ob side indices
                # if the two intersections were on the same side of the obstacle this if will be skipped and the
                # exiting intersection point will be inserted. otherwise we need to insert extra points between
                # the just inserted entrance intersection and the exiting intersection to go around the vertices
                direction_modifier = 0  # variable is set to 1 for CW walks and -1 for CCW to fix area calculations
                if in_oi != out_oi:
                    # do a clockwise walk and calculate the length of the perimeter segment between in and out
                    cw_perim = 0
                    # manually calculate len from intersection to first vertex clockwise
                    cw_perim += utilities.distance_formula(in_intersection.point,
                                                           self.vertices[self.segments[in_oi].cw])
                    # incriment the seg_index variable mod vertex_count to allow circular travel through the list
                    # start at in_oi and add 1 to move CW from the segment we started on.
                    # NOTE adding 1 is equivelant to walking clockwise and subtracting 1 is equivelant to CCW travel
                    seg_index = (in_oi + 1) % self.vertex_count
                    # add the length of each segment which is entirely included in the cw walk
                    while seg_index != out_oi:
                        cw_perim += self.segments[seg_index].len
                        seg_index = (seg_index + 1) % self.vertex_count
                    # manually add the length of the segment between the exit intersection and the point CCW from it
                    cw_perim += utilities.distance_formula(self.vertices[self.segments[out_oi].ccw],
                                                           out_intersection.point)
                    # if the CW walk length is larger than half the total perimeter of self, add points from the CCW
                    # walk, otherwise the CW walk is shorter and we add points from the CW walk
                    if cw_perim > 0.5 * self.perimeter:
                        direction_modifier = -1
                        # add extended vertex points traveling CCW from in to out
                        add_index = in_oi
                        while add_index != out_oi:
                            inserted_points.append(self.vertices[self.segments[add_index].ccw])
                            add_index = (add_index - 1) % self.vertex_count
                    elif cw_perim < 0.5 * self.perimeter:
                        direction_modifier = 1
                        # add points traveling cw from in to out
                        add_index = in_oi
                        while add_index != out_oi:
                            inserted_points.append(self.vertices[self.segments[add_index].cw])
                            add_index = (add_index + 1) % self.vertex_count
                    else:  # TODO debug: (to revert simply make the block above an else and delete this one)
                        print "CW AND CCW EQUAL: Flipping Coin to choose direction..."
                        num = randint(0, 1)
                        if num == 0:
                            direction_modifier = 1
                            print "CW"
                            # add points traveling cw from in to out
                            add_index = in_oi
                            while add_index != out_oi:
                                inserted_points.append(self.vertices[self.segments[add_index].cw])
                                add_index = (add_index + 1) % self.vertex_count
                        else:
                            direction_modifier = -1
                            print "CCW"
                            # add extended vertex points traveling CCW from in to out
                            add_index = in_oi
                            while add_index != out_oi:
                                inserted_points.append(self.vertices[self.segments[add_index].ccw])
                                add_index = (add_index - 1) % self.vertex_count
                # insert the exiting intersection's extended point here
                inserted_points.append(out_intersection.point)

                # calculate polygon area
                area = 0
                origin = Params.area_equator
                # the following line is a hack to fix input 35 and any other future input with an ob below the frame
                # origin[0] -= 0.0000001
                polygon_sides = inserted_points
                deleted_points.reverse()
                polygon_sides.extend(deleted_points)
                # print polygon_sides
                for i in xrange(len(polygon_sides)):
                    # normal area numbers
                    a = polygon_sides[i][0] - origin[0]
                    b = polygon_sides[(i + 1) % len(polygon_sides)][0] - origin[0]
                    h = polygon_sides[(i + 1) % len(polygon_sides)][1] - polygon_sides[i][1]
                    a *= utilities.meters_per_degree
                    b *= utilities.meters_per_degree
                    h *= utilities.meters_per_degree
                    # calculate normal area
                    if h > 0:
                        # add
                        area += ((a + b) / 2) * abs(h) * direction_modifier
                    elif h < 0:
                        # subtract
                        area -= ((a + b) / 2) * abs(h) * direction_modifier
                # DEBUG - negative intrusion values
                if area < 0:
                    print("\n Area {}   Obs Pt = {}   Waypts = {}".format(area, self.vertices[0], len(path)))
                    intrusion = -1000
                    break
                    # area = 1e8  # falesafe
                intrusion += area
            return intrusion
        else:
            # print "Non-Polygon"
            return 0
