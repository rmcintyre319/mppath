import sys
import time
from copy import deepcopy
from datetime import datetime
from multiprocessing import Process, Queue, JoinableQueue, Array
from random import uniform, seed

import utilities
from inputs import load_input_data
from params import Params
from population import Population

import cPickle as pickle

# import the multiprocessing lib
import mpi4py
from mpi4py import MPI

comm = MPI.COMM_WORLD
total = comm.Get_size() - 1
rank = comm.Get_rank()

# import graphics library conditionally so that program continues to
# run if the library is not present
GRAPHICS_LIB_AVAIL = False # CHANGE BACK TO TRUE WHEN DONE
try:
    from graphics import *
except ImportError:
    GRAPHICS_LIB_AVAIL = False
    print "Graphics lib not available.  Will run without graphics output."


# set the GRAPHICS flag
def set_graphics(is_preference):
    if not GRAPHICS_LIB_AVAIL or not is_preference:
        Params.GRAPHICS = False
    else:
        Params.GRAPHICS = True


# Create a circle representing start point, end point, an
# obstacle, a target or a waypoint.
# The scale_factor is a sort-of zoom factor and is specific
# to the size of the window and the obstacle courses on the
# Green.  #FIX THIS -- generalize
def create_circle(circle, c_color):
    scale_factor = Params.scale_factor
    x, y = utilities.get_meters(circle[0], circle[1], Params.origin)
    c = Circle(Point(y * scale_factor, x * scale_factor), circle[2] * scale_factor)
    c.setFill(c_color)
    return c


# Create a line segment representing an obstacle.
# The scale_factor constant is a sort-of zoom factor and is specific
# to the size of the window and the obstacle courses on the
# Green.  #FIX THIS -- generalize
def create_line_seg(ls, l_color):
    scale_factor = Params.scale_factor
    x1, y1 = utilities.get_meters(ls[0], ls[1], Params.origin)
    x2, y2 = utilities.get_meters(ls[2], ls[1], Params.origin)
    l = Line(Point(y1 * scale_factor, x1 * scale_factor), Point(y2 * scale_factor, x2 * scale_factor))
    l.setOutline(l_color)
    return l


# Create a polygon representing an obstacle.
# The scale_factor constant is a sort-of zoom factor and is specific
# to the size of the window and the obstacle courses on the
# Green.  #FIX THIS -- generalize
def create_polygon(p, p_color):
    scale_factor = Params.scale_factor
    p_vertices = []
    k = 0
    while k < len(p):
        x, y = utilities.get_meters(p[k], p[k + 1], Params.origin)
        p_vertices.append(Point(y * scale_factor, x * scale_factor))
        k += 2
    p = Polygon(p_vertices)
    p.setFill(p_color)
    return p


# Write information to the window.  Return the Text
# object so that the information can be written to it
# from within runGA.  Default color is black but other
# colors can be passed in.
def write_text(win, pos, text, c='black'):
    # pos = Point(10, 700)
    t = Text(pos, text)
    t.setText(text)
    t.setSize(16)
    t.setTextColor(c)
    t.draw(win)
    return t


# Create the graphics window and populate it with the start point,
# end point, obstacles and targets.
def make_graphics_window():
    x_size = 750
    y_size = 750
    win = GraphWin("AirMox Pathfinder", x_size, y_size, autoflush=False)
    # win.setBackground('green')
    win.setBackground('white')
    win.setCoords(0, 0, x_size, y_size)
    win.setCoords(0, 0, x_size, y_size)

    # Draw the start and end points.  Put them in a list so that
    # we can draw a line between them in graphics_display.
    # This function returns that list.
    start_end = []
    c = create_circle([Params.start_point[0], Params.start_point[1], 1], 'black')
    c.draw(win)
    start_end.append(c)
    c = create_circle([Params.end_point[0], Params.end_point[1], 1], 'black')
    c.draw(win)
    start_end.append(c)

    # draw the obstacles - obstacle type can be determined by the length of
    # the list representing the obstacle
    for obs in Params.obstacles:
        if obs.type == "C":
            o = create_circle(obs.legacy_data, 'gray')
        elif obs.type == "L":
            o = create_line_seg(obs.legacy_data, 'gray')
        else:  # obs.type == "P"
            o = create_polygon(obs.legacy_data, 'gray')
        o.draw(win)

    # draw the targets - each target is simply a circle of radius 1
    for goal in Params.goals:
        c = create_circle(goal, 'black')
        c.draw(win)

    # Write the labels for number of generations, distance, and number of members displayed
    pos = Point(70, 725)
    write_text(win, pos, "Generation: ")
    pos = Point(60, 690)
    write_text(win, pos, "Distance: ")
    pos = Point(60, 655)
    write_text(win, pos, "Members: ")

    return win, start_end


# Draw the waypoint to waypoint path segments.
# Returns a list of the segments so that they can be undrawn later
# when it is time to draw the next sequence of points and segments.
def draw_path(win, circle_list, color, width):
    graph_lines = []
    for k in xrange(len(circle_list) - 1):
        l = Line(circle_list[k].getCenter(), circle_list[k + 1].getCenter())
        l.setFill(color)
        l.setWidth(width)
        l.draw(win)
        graph_lines.append(l)

    return graph_lines


# Draw the waypoints, not including start point and end point.
# The incoming waypoints are specified by GPS coordinates.
# Returns the list of circles created so that they can be undrawn
# later when it is time to draw the next sequence of points and
# segments.
def draw_waypoints(win, waypoints, color):
    graph_wps = []
    for k, wp in enumerate(waypoints):
        c = create_circle([wp[0], wp[1], 1], color)  # radius previously 0.6 TODO
        if 0 < k < len(waypoints) - 1:
            c.draw(win)
        graph_wps.append(c)
    return graph_wps


# Take front0, sort it to get the best member and then
# draw the waypoints.
# This function is run in a separate thread.
def graphics_display():
    # Create the graphics window for displaying the path during calculation
    # first_points is start and end as graph circles -- needed so that we can
    # draw the naive path between them
    path_window, first_points = make_graphics_window()

    # array of colors for display
    member_colors = ['green', 'aqua', 'yellow', 'red', 'fuchsia']
    # set colors for messages and paths
    color_valid = 'blue'
    color_invalid = 'red'
    color_best = 'lime'

    # postitions for generations and distnace displays in the graphics window
    pos_gen = Point(160, 725)
    pos_dist = Point(165, 690)
    pos_members = Point(160, 655)

    # write generations and distance to the text
    # initial values
    generations = 0
    members = 0

    # format distance to display exactly 2 decimal places
    distance = float(format(utilities.distance_formula(Params.start_point, Params.end_point), '.2f'))
    gen_text = write_text(path_window, pos_gen, str(generations))
    dist_text = write_text(path_window, pos_dist, str(distance))
    members_text = write_text(path_window, pos_members, str(members))

    # text to indicate if current solution is valid
    pos_valid = Point(660, 725)
    valid_text = write_text(path_window, pos_valid, "NOT Valid", color_invalid)

    pos_refine = Point(685, 700)
    refine_text = write_text(path_window, pos_refine, "", 'green')

    graph_lines = draw_path(path_window, first_points, 'red', 5)
    graph_lines_list = [graph_lines]

    graph_points = []
    graph_points_list = [graph_points]

    update()

    # Get the best member of the next generation
    # to draw.  Will block if data not yet available.
    # genes are the waypoints.
    list_of_members = graphics_q.get()

    while list_of_members is not None:
        # get the generation number to display
        generations = graphics_q.get()
        # get the valid flag
        valid_soln = graphics_q.get()
        # get the flag for refine mode
        refine_mode = graphics_q.get()

        # undraw the previous waypoints
        for graph_points in graph_points_list:
            for p in graph_points:
                p.undraw()
        del graph_points_list

        # undraw the previous path segments
        for graph_lines in graph_lines_list:
            for l in graph_lines:
                l.undraw()
        del graph_lines_list

        # update the generation number and distance in display
        gen_text.setText(str(generations))

        # Display distance with 2 decimal places of precision
        assert len(list_of_members) > 0  # TODO
        distance = float(format(list_of_members[0].objectives[Params.path_len], '.2f'))
        dist_text.setText(str(distance))

        # update the number of members and display it
        members = len(list_of_members)
        members_text.setText(str(members))

        # update the valid solution display -- change color if necessary
        if valid_soln:
            valid_text.setTextColor(color_valid)
            valid_text.setText("    Valid")
        else:
            valid_text.setTextColor(color_invalid)
            valid_text.setText("NOT Valid")

        if refine_mode:
            refine_text.setText("Refining")
        else:
            refine_text.setText("")

        graph_points_list = []
        graph_lines_list = []
        for member in reversed(list_of_members):
            # Draw and save the new waypoints and path segments
            '''
            if utilities.is_valid(member):
                path_color = color_valid
            else:
                path_color = color_invalid
            '''
            if member.front < len(member_colors):
                path_color = member_colors[member.front]
            else:
                path_color = 'gray'

            # if i != 0:
            graph_points = draw_waypoints(path_window, member.genes, 'white')
            graph_points_list.append(graph_points)

            graph_lines = draw_path(path_window, graph_points, path_color, 1)
            graph_lines_list.append(graph_lines)

        # draw best member last so it is visable
        graph_points = draw_waypoints(path_window, list_of_members[0].genes, 'yellow')
        graph_points_list.append(graph_points)

        graph_lines = draw_path(path_window, graph_points, color_best, 3)
        graph_lines_list.append(graph_lines)

        update()

        # Get the waypoints to draw
        # Will block if data not yet available
        # Value will be none if no more generations
        list_of_members = graphics_q.get()

    # wait for a value (true) to show up on the queue before closing the window
    # the value is sent from close_window() (called from fly_path)
    graphics_q.get()

    if main_run:
        path_window.getMouse()  # Pause to view result
        path_window.close()  # Close window when done


# Allow exit of the graphics loop and terminate the graphics process.
# The graphics window is actually closed in graphics_display by a 
# graphics library call
def close_window():
    # exit the graphics loop
    graphics_q.put(None)
    # this item on the queue will allow the getMouse and close calls to trigger
    graphics_q.put(True)
    time.sleep(1)
    graphics_p.join()


# The main method running the genetic algorithm. TODO top of run_ga
def run_ga(max_generations, population_size):
    generations = 0
    Params.crossover_count = 0
    Params.mutation_count = 0
    found_valid_soln = False
    valid_member_objs = []

    # generate the list of currently used objectives: Params.objs_used
    utilities.make_obj_list()

    '''
    print "Objectives array:"
    for i in Params.objs_array:
        print ("    {0}".format(i))
    print
    '''

    # flag so that we can detect when a change from invalid to valid occurs    
    prev_valid = False

    threshold10 = max_generations       # for testing: to store generation when path goes within 10% of threshold
    valid_gen = max_generations         # for testing: to store generation when solution becomes valid
    threshold5 = max_generations        # gen when reached early_termination_threshold

    global in_refine_mode               # use the globally defined value

    # if recording a screen capture, pause to allow time to start recording
    # after launching pathfinder
    if record_video:
        time.sleep(video_pause)

    seed(Params.random_seed_pop)  # seed for parent/children creation

    # create a random population
    parents = Population()
    # the following modification was made on 1/16/2017 to ensure that the "equator" path was always in the seed
    mp = utilities.midpoint_formula(Params.start_point, Params.end_point)
    from cell import Cell
    equator = Cell([Params.start_point, mp, Params.end_point])
    parents.population.append(equator)
    # the variable passed into add_random was modified by -1 to account for the additional member added above
    parents.add_random(population_size)

    if not main_run:  # reseed randomly for testing purposes
        seed(Params.random_seed_run)

    # simplify all paths
    parents.simplify_all()

    # evaluate each member of the parent population
    # MULTIPROC THIS EVENTUALLY
    for cell in parents.population:
        cell.evaluate_path()

    # sort by domination fronts
    parents.fast_nondomination_sort()

    # Send a member of front[0] to the graphics process.
    # This is done by putting the front on the interprocess
    # queue so that the graphics process has access to it.
    # Because this is the first generation, it isn't important
    # which member is sent.
    if Params.GRAPHICS:
        # graphics_q.put(parents.list_of_fronts[0]) TODO this statment was replaced for debugging purposes
        graphics_q.put(parents.population)
        graphics_q.put(generations)
        graphics_q.put(False)               # for valid solution
        graphics_q.put(False)               # for refine mode

    # crowding is not calculated for the first set of children

    # use a binary tournament to make a set of children
    children = Population()
    crossover = Population()

    while len(children.population) < 2 * len(parents.population):
        rand_roll = uniform(0, 1)
        if rand_roll <= Params.p_xo and Params.xo_type != 0:
            # use crossover
            p1 = parents.binary_tournament()
            p2 = parents.binary_tournament()
            child1, child2 = p1.x_over(p2)
            # child1, child2 = parents.binary_tournament().x_over(parents.binary_tournament())
            if len(child1.genes) > 4:
                children.population.append(child1)
            if len(child2.genes) > 4:
                children.population.append(child2)
        else:
            # use only mutation
            child = parents.binary_tournament().mutate()
            if len(child.genes) > 4:
                children.population.append(child)

    assert len(parents.population) <= len(children.population)
    crossover.population = deepcopy(children.population)
    children.simplify_all()

    # print output
    time_stamp = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    output_string = "Gen: {0:4d} | {1} | Path Length: {2:8.2f}".format(generations, time_stamp, parents.population[0].objectives[Params.path_len])
    print output_string

    # create combined population
    combined_population = Population()

    # main loop
    # terminate when:
    #           maximum generation reached or
    #           valid solution and distance threshold reached and refinement phase complete
    # Refinement phase requires program to continue for Params.refine_gens generations after
    # a valid, 5% solution found.
    assert len(parents.population) > 0  # TODO
    while generations < max_generations and \
            (not utilities.is_valid(parents.population[0]) or \
            parents.population[0].objectives[Params.path_len] > Params.early_termination_threshold or \
            (generations - threshold5) < Params.refine_gens):

        generations += 1

        # remove evaluation data from last loop
        parents.clear_domination_values()
        children.clear_domination_values()

        # update combined population
        del combined_population
        combined_population = Population()
        combined_population.population = deepcopy(parents.population)  # edited 7/13/16
        # evaluate the children paths
        if MULTIPROC:
            # Divide the child population in NUMBER_OF_PROCS pieces
            # Each is evaluated in a separate process
            pop_lengths = []
            buffer_sizes = []
            for k in xrange(NUMBER_OF_PROCS + 1): # initialize buffer_sizes
                buffer_sizes.append(0)

            for k in xrange(NUMBER_OF_PROCS):
                low = int((float(k) / NUMBER_OF_PROCS) * len(children.population))
                high = int((float(k + 1) / NUMBER_OF_PROCS) * len(children.population))
                pop_lengths.append(high - low)
                to_send = pickle.dumps(children.population[low:high], 2)
                buffer_sizes[k+1] = len(to_send) + 10
                send = comm.isend(buffer_sizes[k+1], dest=k+1)
                send = comm.isend(to_send, dest=k+1)
                #send.wait()

            # Wait for the child_eval to conclude in each process
            for k in xrange(NUMBER_OF_PROCS):
                if (mpi4py.__version__ == '1.3.1'):
                    rec = comm.irecv([buffer_sizes[k+1], int], dest=k+1)
                    rec.wait()
                    rec = comm.irecv([buffer_sizes[k+1], int], dest=k+1)
                else:
                    rec = comm.irecv(source=k + 1)
                    buffer_sizes[k + 1] = rec.wait()
                    rec = comm.irecv(buffer_sizes[k+1], source=k+1)
                to_extend = pickle.loads(rec.wait())
                combined_population.population.extend(to_extend)


            #for k in xrange(NUMBER_OF_PROCS):
            #    combined_population.population.extend(return_q.get())
            #eval_q.join()
        else:
            # if not MULTIPROC, child evaluation happens in this process
            for cell in children.population:
                cell.evaluate_path()

            # add children to the combined population
            combined_population.population.extend(children.population)

        # TODO TEST CODE
        # periodically make all members valid and reevaluate
        if Params.make_valid and (generations % Params.make_valid_interval == (Params.make_valid_interval - 1)):
            for cell in combined_population.population:
                cell.make_valid()
                cell.evaluate_path()

        # assign fronts to the combined pop
        combined_population.fast_nondomination_sort()

        # reassign parents to be the top N members of the combined pop
        del parents.population[:]
        parents.population = combined_population.top_n(population_size)

        '''
        # TODO debugging for large number of waypoints
        # Prints data on number waypoints: average, max, number >= 0.9*max, number == 0
        if generations % 100 == 0:
            total_waypoints = 0
            max_waypoints = 0
            big_count = -1
            num_zero = 0
            for member in parents.population:
                num_waypoints = member.objectives[Params.waypoint_count]
                total_waypoints += num_waypoints
                if num_waypoints > max_waypoints:
                    max_waypoints = num_waypoints
                elif num_waypoints == 0:
                    num_zero += 1
            for member in parents.population:
                if (member.objectives[Params.waypoint_count]) >= 0.9 * max_waypoints:
                    big_count += 1
            print
            print
            print("Parents:")
            print("Avg waypts: {0}     Max waypts: {1}     Count big: {2}     Num zero: {3}".format(
                total_waypoints / len(parents.population), max_waypoints, big_count, num_zero))


            # TODO debugging for large number of waypoints
            total_waypoints = 0
            max_waypoints = 0
            big_count = -1
            for member in children.population:
                num_waypoints = member.objectives[Params.waypoint_count]
                total_waypoints += num_waypoints
                if num_waypoints > max_waypoints:
                    max_waypoints = num_waypoints
            for member in children.population:
                if (member.objectives[Params.waypoint_count]) >= 0.9 * max_waypoints:
                    big_count += 1
            print
            print("Children:")
            print("Avg waypts: {0}     Max waypts: {1}     Count big: {2}".format(
                total_waypoints / len(children.population), max_waypoints, big_count))
        '''

        # radix sort, least significant to most significant objectives
        for s in Params.sort_order:
            parents.population.sort(reverse=False, key=lambda cells: cells.objectives[s])

        # median_member = int(population_size / 2)

        found_valid_soln = utilities.is_valid(parents.population[0])

        # record the generation when the solution went valid and
        # record the generation when the soltuion went within 10% of threshold
        # with some versions of extinction, all valid solutions can be lost
        if found_valid_soln:
            if prev_valid != found_valid_soln:
                prev_valid = True
                valid_member_objs = deepcopy(parents.population[0].objectives)

            if valid_gen == max_generations:
                valid_gen = generations
            # if within threshold, set threshold gen to enter refinement phase
            if parents.population[0].objectives[Params.path_len] <= Params.early_termination_threshold:
                if threshold5 == max_generations:
                    threshold5 = generations
                    if Params.refine_gens > 0:
                        in_refine_mode = True
                        print("Refining at generation: {0}".format(threshold5))
            else:
                threshold5 = max_generations
                in_refine_mode = False

            # this is the 10% threshold -- early_termination_threshold already contains 5% padding
            if parents.population[0].objectives[Params.path_len] <= Params.early_termination_threshold * 1.05:
                if threshold10 == max_generations:
                    threshold10 = generations
            else:
                threshold10 = max_generations
        elif prev_valid:
            # If there is no longer a valid solution,
            # reset the generatin at which a valid solution was found
            print("\n Valid Solution Lost")
            print("Valid Member: {0}".format(valid_member_objs))
            print("New Member: {0}".format(parents.population[0].objectives))
            print
            prev_valid = False
            valid_gen = max_generations

        if Params.GRAPHICS:
            # periodically draw waypoints on the graph
            modnum = int(max_generations * 0.01)  # 0.001

            if (modnum == 0) or (generations % modnum == 0):
                # Put a list of members (population or front) on the queue so that
                # the graphics process can display it.
                # ** uncomment exactly one of the following lines to display one of the following:

                # graphics_pop_list = combined_population.population

                # ** first front of the current population
                graphics_pop_list = deepcopy(combined_population.list_of_fronts[0])

                # # ** combined population first 4 fronts
                # graphics_pop_list = combined_population.list_of_fronts[0]
                # graphics_pop_list.extend(combined_population.list_of_fronts[1])
                # graphics_pop_list.extend(combined_population.list_of_fronts[2])
                # graphics_pop_list.extend(combined_population.list_of_fronts[3])

                # ** current child population
                # graphics_pop_list = deepcopy(children.population)

                # ** current children created by crossover
                # graphics_pop_list = deepcopy(crossover.population)

                # TODO - Debugging - display 10 members with most waypoints
                # ** members with most waypoints **
                # graphics_temp_list = deepcopy(combined_population.population)
                # graphics_temp_list.sort(reverse=True, key=lambda cells: cells.objectives[0])
                # graphics_pop_list = deepcopy(graphics_temp_list[:10+1])
                # del graphics_temp_list

                # radix sort, least significant to most significant objectives
                for s in Params.sort_order:
                    graphics_pop_list.sort(reverse=False, key=lambda cells: cells.objectives[s])
                graphics_q.put(graphics_pop_list)
                graphics_q.put(generations)
                graphics_q.put(found_valid_soln)
                graphics_q.put(in_refine_mode)

        # determine if extinction occurs
        if Params.extinction_type != 0 and (
                        generations % Params.interval_ext == 0):  # and (generations < 0.9 * max_generations):
            if uniform(0, 1) < Params.p_ext:
                # reset extinction probability to base value
                Params.p_ext = Params.p_ext_init
                print "Extinction occurred: GEN {}".format(generations)
                parents.extinction()
            else:
                # increment extinction probability
                Params.p_ext += Params.inc_ext

        # clear old and build new child population
        del children.population[:]
        del crossover.population[:]

        while len(children.population) < population_size:
            rand_roll = uniform(0, 1)
            if rand_roll < Params.p_xo and Params.xo_type != 0:
                child1, child2 = parents.binary_tournament().x_over(parents.binary_tournament())
                if len(child1.genes) > 4:
                    children.population.append(child1)
                    crossover.population.append(child1)
                if len(child2.genes) > 4:
                    children.population.append(child2)
                    crossover.population.append(child2)
            else:
                child = parents.binary_tournament().mutate()
                if len(child.genes) > 4:
                    children.population.append(child)

        # delete a child if too many were created
        while len(children.population) > population_size:
            children.population.pop()
        assert 2 * population_size <= len(children.population) + len(parents.population)
        children.simplify_all()

        # print #####
        if generations % 100 == 0:
            # print output
            time_stamp = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
            output_string = "Gen: {0:4d} | {1} | Front 0: {2:4d} | Path Len: {3:8.2f}".format(generations, time_stamp,
                                len(combined_population.list_of_fronts[0]), parents.population[0].objectives[Params.path_len])

            if found_valid_soln:
                output_string += "  Valid"
            print output_string

    # ##########END LOOP##########

    # prep the data for saving to a log

    # remove evaluation data from last loop
    parents.clear_domination_values()
    children.clear_domination_values()

    # evaluate the children paths
    #MULTIPROC THIS EVENTUALLY
    for cell in children.population:
        cell.evaluate_path()

    # create a combined population
    combined_population = Population()
    combined_population.population = deepcopy(parents.population)
    combined_population.population.extend(children.population)

    # assign fronts to the combined pop
    combined_population.fast_nondomination_sort()

    # combined_population.crowding_distance_assignment(0)
    for k in xrange(len(combined_population.list_of_fronts) - 1):
        combined_population.crowding_distance_assignment(k)

        # radix sort, least significant to most significant objectives
        for s in Params.sort_order:
            combined_population.list_of_fronts[k].sort(reverse=False, key=lambda cells: cells.objectives[s])


    if Params.GRAPHICS:
        # put waypoints of last generation on queue
        # graphics_q.put(combined_population.list_of_fronts[0]) TODO replaced for debugging
        graphics_q.put(parents.population)
        graphics_q.put(generations)
        graphics_q.put(found_valid_soln)
        graphics_q.put(False)               # False for in_refine_mode sicne run is complete

    print
    print "Number of crossover events: %s" % Params.crossover_count
    print "Number of mutation events: %s" % Params.mutation_count
    # print "Number of path/obstacle intersections: {}".format(Params.intersection_count_array)

    # CHANGE by HDM on 10-12-2015
    # return combined_population.population[0], generations
    # TODO for debugging the return value is changed to a member from the parent population
    # (combined_population.list_of_fronts[0][0])
    if (MULTIPROC):
        finnish_flag = pickle.dumps("Terminate")
        for k in xrange(NUMBER_OF_PROCS):
            send = comm.isend("Terminate", dest=k+1) # send the finnish flag (pun on finish flag) to the child process

    return found_valid_soln, parents.population[0], threshold5, threshold10, valid_gen


# TODO end run_ga


# Wrapper for run_ga() that runs it for number of attempts specified by
# parameter num_try.
def calculate_waypoints(num_try, pop_size, gens):
    best_member = None

    valid_soln = False

    count = 0

    # Do not make another attempt if max number of tries reached or a valid
    # solution was found
    while not valid_soln and count < num_try:
        # repeat until a path is accepted
        # while not accepted and not terminate:
        print ""
        print "Attempt: {}:".format(count + 1)

        # TODO print objs_maps to make sure proper objectives being used
        # print Params.objs_map_pre
        # print Params.objs_map_post

        valid_soln, best_member, generations, ten_percent, valid_gens = run_ga(gens, pop_size)
        # a valid solution hit 0 obstacles and all goals and has no waypts farther than fence value from start
        # TODO debug print
        print best_member.genes
        print
        if valid_soln:
            print "    Attempt {} is valid:  Gens: {}  Objectives = {}".format(count + 1, generations, best_member.objectives)

        else:
            print "    Attempt {} is NOT valid. Objectives = {}".format(count + 1, best_member.objectives)

        count += 1

    print ""

    return valid_soln, best_member.genes, [valid_gens, ten_percent, generations]


# Create child evaluation processes, graphics process and invoke calculate_waypoints().
# Cleanup child eval processes when done.
# Graphics process is not killed here -- need to do that later so that the graphics
# window can persist until user clicks on it.
def main(num_try, pop_size, gens):
    # set the geofence distance
    utilities.set_max_distance()

    #if MULTIPROC:
    #    processes = []
    #    for j in xrange(NUMBER_OF_PROCS):
    #        processes.append(Process(target=eval_children))
    #        processes[j].start()

    # assign the graphics process and start it
    # put ob_points and goal_points on the queue so they are available to the graphics process
    if Params.GRAPHICS:
        global graphics_p
        graphics_p = Process(target=graphics_display)
        graphics_p.start()

    # capture the time the call is made so that elapsed time can be calculated
    s_time = time.time()
    valid, best_mem, testing_gens = calculate_waypoints(num_try, pop_size, gens)
    # capture the time the call returns
    e_time = time.time()

    #if MULTIPROC:
        # put none on the eval_q for each of the children eval procs
        # so that they will terminate
    #    for i in xrange(NUMBER_OF_PROCS):
    #        eval_q.put(None)

    #    for p in processes:
    #        p.join()

    return valid, best_mem, testing_gens, s_time, e_time


# GLOBAL VALUES

# Flag for use of multiprocessing
MULTIPROC = True
NUMBER_OF_PROCS = total

# flag to allow pause for video recording
record_video = False
video_pause = 15

# global Queues for interprocess communication
graphics_q = Queue()
graphics_p = Process()

# global queues for child evaluation
#if MULTIPROC:
#    eval_q = JoinableQueue()
#    return_q = Queue()

# set to true when refine mode entered
# refine mode gives alg time to refine a valid solution
in_refine_mode = False

main_run = False
if __name__ == '__main__':
    if rank == 0:
        main_run = True

        # read in command line arguments
        numargs = len(sys.argv)
        if numargs == 5:
            cmd_input_num = int(sys.argv[1])
            cmd_pop_size = int(sys.argv[2])
            cmd_gens = int(sys.argv[3])
            Params.random_seed = int(sys.argv[4])
        elif numargs == 4:
            cmd_input_num = int(sys.argv[1])
            cmd_pop_size = int(sys.argv[2])
            cmd_gens = int(sys.argv[3])
            Params.random_seed = None
        else:
            sys.stderr.write("Usage: python {}  input popsize generations [random_seed]\n".format(sys.argv[0]))
            raise SystemExit(1)

        # load data for the input obstacle course based on command line parameter
        load_input_data(cmd_input_num)

        # request graphics if available
        set_graphics(True)

        # main takes number of pathfinding attempts to make,
        # population size and number of generations
        is_valid, best, testing, start_time, end_time = main(1, cmd_pop_size, cmd_gens)

        if Params.GRAPHICS:
            close_window()  # close graphics window
    else:
        load_input_data(int(sys.argv[1]))
        while (True): # Decide on a better loop control. Maybe send a string indicating it should quit? This is listening for every generation, forever
            # receives the child population, evaluates, and does a non-blocking send back to the original process for each cell that is evaluated
            recv = comm.irecv(source=0)
            buffer_size = recv.wait()
            if (buffer_size == "Terminate"):
                print("Terminating on", rank)
                sys.exit()
            recv = comm.irecv(buffer_size, source=0)
            c_pop = pickle.loads(recv.wait())
            repickled = pickle.dumps(c_pop, 2)
            send_queue = []
            for cell in c_pop:
                cell.evaluate_path()
                # print cell.objectives[1]
                send_queue.append(cell)
            to_send = pickle.dumps(send_queue)
            send = comm.isend(len(to_send) + 10, dest=0)
            send = comm.isend(to_send, dest=0)
            send.wait()
            #return_q.put(c_pop)
            #eval_q.task_done()

#Somewhere print out params.objs_array