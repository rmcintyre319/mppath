from inputs import load_input_data
from params import Params
from pathfinder import main
from obstacle import Obstacle
import utilities
import sys
import os
from datetime import datetime
from time import time


def print_test(s, i, t):
    print "\n\n** Series {} | Input {} | Test {} **\n\n".format(s, i, t)

if __name__ == "__main__":

    random_seed = None

    numargs = len(sys.argv)
    if numargs == 2:
        input_num = int(sys.argv[1])
    else:
        sys.stderr.write("Usage: python {} popsize generations (random_seed)\n".format(sys.argv[0]))
        raise SystemExit(1)


    load_input_data(input_num)
    area = Params.obstacles[0].path_intrusion([Params.start_point, Params.end_point])

    print 'area = {0}'.format(area)
    print
